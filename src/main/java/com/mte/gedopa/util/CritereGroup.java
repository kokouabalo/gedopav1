/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HP
 */
public class CritereGroup implements ICritere{
    
    private OperateurSql opSql;
    private List<CritereSimple> criteres = new ArrayList<>();
    
    public CritereGroup(){
        this(OperateurSql.AND);
    }
    
    public CritereGroup(OperateurSql opSql){
        this.opSql = opSql;
    }

    /**
     * @return the opSql
     */
    public OperateurSql getOpSql() {
        return opSql;
    }

    /**
     * @param opSql the opSql to set
     */
    public void setOpSql(OperateurSql opSql) {
        this.opSql = opSql;
    }

    /**
     * @return the criteres
     */
    public List<CritereSimple> getCriteres() {
        return criteres;
    }

    /**
     * @param criteres the criteres to set
     */
    public void setCriteres(List<CritereSimple> criteres) {
        this.criteres = criteres;
    }
    
}
