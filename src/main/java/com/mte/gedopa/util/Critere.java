/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.util;

/**
 *
 * @author HP
 */
public class Critere{
    
    private int id;
    private String propriete;
    private Object valeur;
    private OperateurSql opSql;
    private OperateurCond opCond;

    public Critere(String propriete, Object valeur) {
        this(propriete, OperateurCond.LIKE, valeur, OperateurSql.AND);
    }
    
    public Critere(String propriete, Object valeur, OperateurSql opSql) {
        this(propriete, OperateurCond.LIKE, valeur, opSql);
    }
    
    public Critere(String propriete, OperateurCond opCond, Object valeur) {
        this(propriete, opCond, valeur, OperateurSql.AND);
    }
    
    public Critere(String propriete, OperateurCond opCond, Object valeur, OperateurSql opSql) {
        this.propriete = propriete;
        this.valeur = valeur;
        this.opSql = opSql;
        this.opCond = opCond;
    }

    /**
     * @return the propriete
     */
    public String getPropriete() {
        return propriete;
    }

    /**
     * @param propriete the propriete to set
     */
    public void setPropriete(String propriete) {
        this.propriete = propriete;
    }

    /**
     * @return the valeur
     */
    public Object getValeur() {
        return valeur;
    }

    /**
     * @param valeur the valeur to set
     */
    public void setValeur(Object valeur) {
        this.valeur = valeur;
    }

    /**
     * @return the opSql
     */
    public OperateurSql getOpSql() {
        return opSql;
    }

    /**
     * @param opSql the opSql to set
     */
    public void setOpSql(OperateurSql opSql) {
        this.opSql = opSql;
    }

    /**
     * @return the opCond
     */
    public OperateurCond getOpCond() {
        return opCond;
    }

    /**
     * @param opCond the opCond to set
     */
    public void setOpCond(OperateurCond opCond) {
        this.opCond = opCond;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
   
}
