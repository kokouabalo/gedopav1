/**
 * Cette interface contient toutes les constantes numériques et chaînes de caractères
 */
package com.mte.gedopa.util;

/**
 * @author Horacio TONOU (DG SI Consult)
 *
 */
public interface IConstants {

    public static final String PDF_FORMAT = "pdf";
    public static final String HTML_FORMAT = "html";
    public static final String XLS_FORMAT = "xls";
    public static final String XLSX_FORMAT = "xlsx";
    public static final String CSV_FORMAT = "csv";
    public static final String WORD_FORMAT = "doc";
    public static final String EXPORT_DOCS_PATH = "EXPORT_DOCS_PATH";
    public static final String SUBREPORT_DIR = "SUBREPORT_DIR";
}
