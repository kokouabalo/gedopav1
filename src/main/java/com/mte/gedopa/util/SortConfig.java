/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.util;

/**
 *
 * @author HP
 */
public class SortConfig{
    
    protected int id;
    protected String propriete;
    protected SortOrder sortOrder;

    public SortConfig(String propriete) {
        this(propriete, SortOrder.ASC);
    }
    
    public SortConfig(String propriete, SortOrder sortOrder) {
        this.propriete = propriete;
        this.sortOrder = sortOrder;
    }

    /**
     * @return the propriete
     */
    public String getPropriete() {
        return propriete;
    }

    /**
     * @param propriete the propriete to set
     */
    public void setPropriete(String propriete) {
        this.propriete = propriete;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the sortOrder
     */
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /**
     * @param sortOrder the sortOrder to set
     */
    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
   
}
