/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.util;

import java.math.BigDecimal;
//import org.mte.montantlettres.Montant;

/**
 *
 * @author CLAUDE AGBODJAN
 */
public class OperationCalcul {

    public static String ConcatNumeroFacture(String exe, String cat, String num) {

        String numfact;
        numfact = exe + cat + num;

        return numfact;
    }

    public static BigDecimal calculerVolumeNavire(BigDecimal longueur, BigDecimal largeur, BigDecimal te) {

        BigDecimal teTheor;
        BigDecimal volume;

        teTheor = BigDecimal.valueOf(Math.sqrt((longueur.multiply(largeur)).doubleValue())).multiply(new BigDecimal("0.14"));

        if (teTheor.doubleValue() > te.doubleValue()) {

            volume = longueur.multiply(largeur).multiply(teTheor);

        } else {
            volume = longueur.multiply(largeur).multiply(te);

        }

        return volume;
    }

    public static BigDecimal calculerMontantDetail(BigDecimal qte, BigDecimal tarif, BigDecimal base, BigDecimal chargeFixe, BigDecimal majoration, BigDecimal txRem, BigDecimal MttRem, boolean forfaitBar, boolean forfaitSup) {

        BigDecimal montant = BigDecimal.ZERO;

        if (forfaitBar) { // forfait de bareme (ne pas tenir compte de la qté à facturer)
            if (forfaitSup) { // // utiliser  majoration comme un montant à ajouter au montant détail
                montant = ((tarif.add(majoration)).multiply(BigDecimal.ONE.subtract(txRem.divide(new BigDecimal("100")))).multiply(base)).add(chargeFixe).subtract(MttRem);

            } else { // utiliser  majoration comme un % à calculer sur le tarif

                montant = ((tarif.add(tarif.multiply(majoration.divide(new BigDecimal("100"))))).multiply(BigDecimal.ONE.subtract(txRem.divide(new BigDecimal("100")))).multiply(base)).add(chargeFixe).subtract(MttRem);

                   }

        } else // pas de forfait de bareme (tenir compte de la qté à facturer)
        {
            if (forfaitSup) {// utiliser  majoration comme un montant à ajouter au montant détail

                montant = ((qte.multiply(tarif).add(majoration)).multiply(BigDecimal.ONE.subtract(txRem.divide(new BigDecimal("100")))).multiply(base)).add(chargeFixe).subtract(MttRem);;

            } else {

                montant = (((qte.multiply(tarif)).add((qte.multiply(tarif)).multiply(majoration.divide(new BigDecimal("100"))))).multiply(BigDecimal.ONE.subtract(txRem.divide(new BigDecimal("100")))).multiply(base)).add(chargeFixe).subtract(MttRem);
               
            }
        }

        return montant;
    }

    public static BigDecimal arrondiNDecimales(BigDecimal x, int n) {
        double pow = Math.pow(10, n);
        return BigDecimal.valueOf((Math.floor(x.doubleValue() * pow)) / pow);
    }

    public static BigDecimal arrondiValeur(BigDecimal montant, BigDecimal precision) {

        BigDecimal arrondi = BigDecimal.ZERO;
        if (precision.doubleValue() == 0) {
            precision = BigDecimal.ONE;
        }
        if (montant == BigDecimal.ZERO) {
            arrondi = BigDecimal.ZERO;
        } else if (montant.doubleValue() < precision.doubleValue()) {

            arrondi = precision.subtract(montant);
        } else {
            BigDecimal reste = montant.remainder(precision);
            if (reste.doubleValue() > 0) {
                arrondi = precision.subtract(reste);
            }
        }

        return arrondi;
    }

    public static String trimFront(String text, char character) {
        String normalizedText;
        int index;

        if (null == text || text.isEmpty()) {
            return text;
        }

        normalizedText = text.trim();
        index = 0;

        while (normalizedText.charAt(index) == character) {
            index++;
        }
        return normalizedText.substring(index).trim();
    }

    /**
     *
     * @param text Text
     * @param character Character to remove
     * @return
     */
    public static String trimEnd(String text, char character) {
        String normalizedText;
        int index;

        if (null == text || text.isEmpty()) {
            return text;
        }

        normalizedText = text.trim();
        index = normalizedText.length() - 1;

        while (normalizedText.charAt(index) == character) {
            if (--index < 0) {
                return "";
            }
        }
        return normalizedText.substring(0, index + 1).trim();
    }

    /**
     *
     * @param text Text
     * @param character Character to remove
     * @return
     */
    public static String trimAll(String text, char character) {
        String normalizedText = trimFront(text, character);

        return trimEnd(normalizedText, character);
    }

    public static String montantEnLettre(BigDecimal mont, String dev) {
        String enLettre;
        String firstDecimalZero = "";
        char zero = '0';

        // mont = arrondiNDecimales(mont, 2);
        String montantString;
        Long montant0;
        Long montant1;
        montantString = mont.toPlainString();
        if (montantString.indexOf(".") != -1) { // contient "."
            String[] partP = montantString.split("[.]");

            String montantEntier = partP[0];
            String montantDecimal = partP[1];
            //supprimer les zéro non significatif à la fin du montant décimal
            if (new BigDecimal(montantDecimal).doubleValue() > 0) {
                montantDecimal = trimEnd(montantDecimal, zero);
            }

            if (montantDecimal.substring(0, 1).equalsIgnoreCase("0")) {
                firstDecimalZero = "Zéro ";
            }
            montant0 = Long.valueOf(montantEntier);
            montant1 = Long.valueOf(montantDecimal);
        } else {
            montant0 = Long.valueOf(montantString);
            montant1 = new Long(0);
        }

        if (montant0 > 0) {
            if (montant1 == 0) {
//                enLettre = (new Montant(BigDecimal.valueOf(montant0))).getLettres() + " " + dev;
            } else {
   //             enLettre = (new Montant(BigDecimal.valueOf(montant0))).getLettres() + " " + dev + " " + firstDecimalZero + (new Montant(BigDecimal.valueOf(montant1))).getLettres();
            }
        } else {
            if (montant1 == 0) {
                enLettre = "Zéro" + " " + dev;
            } else {
//                enLettre = "Zéro" + " " + dev + " " + firstDecimalZero + (new Montant(BigDecimal.valueOf(montant1))).getLettres();
            }
        }

  //      return enLettre;
  return " ";// A revoir après
    }

    public static void convertDecimalEnLettre(BigDecimal deb, BigDecimal fin, BigDecimal pas) {

        BigDecimal tempVal;
        if (deb.doubleValue() > fin.doubleValue()) {
            tempVal = deb;
            deb = fin;
            fin = tempVal;
        }

        if (pas.doubleValue() <= 0) {
            pas = BigDecimal.ONE;
        }
        ////////////
        while (deb.doubleValue() <= fin.doubleValue()) {
            String enLettre = montantEnLettre(deb, "");
            System.out.println(deb + " (En Lettre) : " + enLettre);
            deb = deb.add(pas);
        }
    }
}
