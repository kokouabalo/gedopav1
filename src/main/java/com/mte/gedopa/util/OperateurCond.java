/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.util;
/**
 *
 * @author GiGi
 */
public enum OperateurCond {
    
    EQUAL("="),NOT_EQUAL("<>"), LESS_THAN("<"), LESS_OR_EQUAL("<="), GREATER_THAN(">"), GREATER_OR_EQUAL(">="), LIKE("LIKE"), NOT_LIKE("NOT LIKE");
    String equivSql;
    
    OperateurCond(String equivSql){
        this.equivSql = equivSql;
    }

    @Override
    public String toString() {
        return equivSql;
    }
    
    
}
