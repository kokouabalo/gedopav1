/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "MEDECIN")
//@SequenceGenerator(name="medecin_idmed_seq", allocationSize = 1)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medecin.findAll", query = "SELECT a FROM Medecin a")})
public class Medecin implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    @Id
    //@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="medecin_idmed_seq")
    @Basic(optional = false)
    @Column(name = "idmed")
    private String idmed;
    @Column(name = "nommedecin")
    private String nommedecin;
    @Column(name = "prenommedecin")
    private String prenommedecin;
    @Column(name = "adrmedecin")
    private String adrmedecin;
    @Column(name = "telmedecin")
    private String telmedecin;
    @Column(name = "emailmedecin")
    private String emailmedecin;
    
    @JoinColumn(name = "idspec", referencedColumnName = "idspec")
    @ManyToOne(optional = false)
    private Specialite idspec;
   
    public Medecin() {
    }

    public Medecin(String idmed) {
        this.idmed = idmed;
    }

    public Medecin(String idmed, String nommedecin, String prenommedecin, String adrmedecin, String telmedecin, String emailmedecin, Specialite idspec) {
        this.idmed = idmed;
        this.nommedecin = nommedecin;
        this.prenommedecin = prenommedecin;
        this.adrmedecin = adrmedecin;
        this.telmedecin = telmedecin;
        this.emailmedecin = emailmedecin;
        this.idspec = idspec;
    }

    /**
     * @return the idmed
     */
    public String getIdmed() {
        return idmed;
    }

    /**
     * @param idmed the idmed to set
     */
    public void setIdmed(String idmed) {
        this.idmed = idmed;
    }

    /**
     * @return the nommedecin
     */
    public String getNommedecin() {
        return nommedecin;
    }

    /**
     * @param nommedecin the nommedecin to set
     */
    public void setNommedecin(String nommedecin) {
        this.nommedecin = nommedecin;
    }

    /**
     * @return the prenommedecin
     */
    public String getPrenommedecin() {
        return prenommedecin;
    }

    /**
     * @param prenommedecin the prenommedecin to set
     */
    public void setPrenommedecin(String prenommedecin) {
        this.prenommedecin = prenommedecin;
    }

    /**
     * @return the adrmedecin
     */
    public String getAdrmedecin() {
        return adrmedecin;
    }

    /**
     * @param adrmedecin the adrmedecin to set
     */
    public void setAdrmedecin(String adrmedecin) {
        this.adrmedecin = adrmedecin;
    }

    /**
     * @return the telmedecin
     */
    public String getTelmedecin() {
        return telmedecin;
    }

    /**
     * @param telmedecin the telmedecin to set
     */
    public void setTelmedecin(String telmedecin) {
        this.telmedecin = telmedecin;
    }

    /**
     * @return the emailmedecin
     */
    public String getEmailmedecin() {
        return emailmedecin;
    }

    /**
     * @param emailmedecin the emailmedecin to set
     */
    public void setEmailmedecin(String emailmedecin) {
        this.emailmedecin = emailmedecin;
    }

    /**
     * @return the idspec
     */
    public Specialite getIdspec() {
        return idspec;
    }

    /**
     * @param idspec the idspec to set
     */
    public void setIdspec(Specialite idspec) {
        this.idspec = idspec;
    }
   
    @Override
    public String toString() {
       return getClass().getSimpleName() + " :: ID = " + getIdmed();
    }
    
}
