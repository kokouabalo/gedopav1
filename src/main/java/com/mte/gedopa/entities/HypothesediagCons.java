/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "hypothesediag_cons")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HypothesediagCons.findAll", query = "SELECT f FROM HypothesediagCons f")})
public class HypothesediagCons implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private HypothesediagConsPK hypothesediagConsPK;
    @Basic(optional = false)
    @Column(name = "observationhypo")
    private String observationhypo;
    @JoinColumn(name = "idhypo", referencedColumnName = "idhypo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Hypothesediag hypothesediag;
    @JoinColumn(name = "idconsultaion", referencedColumnName = "idconsultaion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Consultation consultation;

    public HypothesediagCons() {
    }

    public HypothesediagCons(HypothesediagConsPK hypothesediagConsPK) {
        this.hypothesediagConsPK = hypothesediagConsPK;
    }

    public HypothesediagCons(HypothesediagConsPK hypothesediagConsPK, String observationhypo) {
        this.hypothesediagConsPK = hypothesediagConsPK;
        this.observationhypo = observationhypo;
    }

    public HypothesediagCons(String idhypo, long idconsultaion) {
        this.hypothesediagConsPK = new HypothesediagConsPK(idhypo, idconsultaion);
    }

    public HypothesediagConsPK getHypothesediagConsPK() {
        return hypothesediagConsPK;
    }

    public void setHypothesediagConsPK(HypothesediagConsPK hypothesediagConsPK) {
        this.hypothesediagConsPK = hypothesediagConsPK;
    }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getHypothesediagConsPK() != null ? getHypothesediagConsPK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HypothesediagCons)) {
            return false;
        }
        HypothesediagCons other = (HypothesediagCons) object;
        if ((this.getHypothesediagConsPK() == null && other.getHypothesediagConsPK() != null) || (this.getHypothesediagConsPK() != null && !this.hypothesediagConsPK.equals(other.hypothesediagConsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + getHypothesediagConsPK();
    }

    /**
     * @return the observationhypo
     */
    public String getObservationhypo() {
        return observationhypo;
    }

    /**
     * @param observationhypo the observationhypo to set
     */
    public void setObservationhypo(String observationhypo) {
        this.observationhypo = observationhypo;
    }

    /**
     * @return the hypothesediag
     */
    public Hypothesediag getHypothesediag() {
        return hypothesediag;
    }

    /**
     * @param hypothesediag the hypothesediag to set
     */
    public void setHypothesediag(Hypothesediag hypothesediag) {
        this.hypothesediag = hypothesediag;
    }

    /**
     * @return the consultation
     */
    public Consultation getConsultation() {
        return consultation;
    }

    /**
     * @param consultation the consultation to set
     */
    public void setConsultation(Consultation consultation) {
        this.consultation = consultation;
    }
    
}
