/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "analyse_demandee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnalyseDemandee.findAll", query = "SELECT f FROM AnalyseDemandee f")})
public class AnalyseDemandee implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private AnalyseDemandeePK analyseDemandeePK;
    @Basic(optional = false)
    @Column(name = "resultatana")
    private String resultatana;
    @JoinColumn(name = "idanamed", referencedColumnName = "idanamed", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Analysemed analysemed;
    @JoinColumn(name = "idconsultaion", referencedColumnName = "idconsultaion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Consultation consultation;

    public AnalyseDemandee() {
    }

    public AnalyseDemandee(AnalyseDemandeePK analyseDemandeePK) {
        this.analyseDemandeePK = analyseDemandeePK;
    }

    public AnalyseDemandee(AnalyseDemandeePK analyseDemandeePK, String resultatana) {
        this.analyseDemandeePK = analyseDemandeePK;
        this.resultatana = resultatana;
    }

    public AnalyseDemandee(String idanamed, long idconsultaion) {
        this.analyseDemandeePK = new AnalyseDemandeePK(idanamed, idconsultaion);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getAnalyseDemandeePK() != null ? getAnalyseDemandeePK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalyseDemandee)) {
            return false;
        }
        AnalyseDemandee other = (AnalyseDemandee) object;
        if ((this.getAnalyseDemandeePK() == null && other.getAnalyseDemandeePK() != null) || (this.getAnalyseDemandeePK() != null && !this.analyseDemandeePK.equals(other.analyseDemandeePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + getAnalyseDemandeePK();
    }

    /**
     * @return the analyseDemandeePK
     */
    public AnalyseDemandeePK getAnalyseDemandeePK() {
        return analyseDemandeePK;
    }

    /**
     * @param analyseDemandeePK the analyseDemandeePK to set
     */
    public void setAnalyseDemandeePK(AnalyseDemandeePK analyseDemandeePK) {
        this.analyseDemandeePK = analyseDemandeePK;
    }

    /**
     * @return the resultatana
     */
    public String getResultatana() {
        return resultatana;
    }

    /**
     * @param resultatana the resultatana to set
     */
    public void setResultatana(String resultatana) {
        this.resultatana = resultatana;
    }

    /**
     * @return the analysemed
     */
    public Analysemed getAnalysemed() {
        return analysemed;
    }

    /**
     * @param analysemed the analysemed to set
     */
    public void setAnalysemed(Analysemed analysemed) {
        this.analysemed = analysemed;
    }

    /**
     * @return the consultation
     */
    public Consultation getConsultation() {
        return consultation;
    }

    /**
     * @param consultation the consultation to set
     */
    public void setConsultation(Consultation consultation) {
        this.consultation = consultation;
    }
    
}
