/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "PROFIL_MENU", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProfilMenu.findAll", query = "SELECT p FROM ProfilMenu p")})
public class ProfilMenu implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProfilMenuPK profilMenuPK;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "PROFIL_CODE", referencedColumnName = "PROFIL_CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Profil profil;
    @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menu menu;

    public ProfilMenu() {
    }

    public ProfilMenu(ProfilMenuPK profilMenuPK) {
        this.profilMenuPK = profilMenuPK;
    }

    public ProfilMenu(ProfilMenuPK profilMenuPK, Boolean actif) {
        this.profilMenuPK = profilMenuPK;
        this.actif = actif;
    }

    public ProfilMenu(int menuId, String profilCode) {
        this.profilMenuPK = new ProfilMenuPK(menuId, profilCode);
    }

    public ProfilMenuPK getProfilMenuPK() {
        return profilMenuPK;
    }

    public void setProfilMenuPK(ProfilMenuPK profilMenuPK) {
        this.profilMenuPK = profilMenuPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profilMenuPK != null ? profilMenuPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfilMenu)) {
            return false;
        }
        ProfilMenu other = (ProfilMenu) object;
        if ((this.profilMenuPK == null && other.profilMenuPK != null) || (this.profilMenuPK != null && !this.profilMenuPK.equals(other.profilMenuPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + profilMenuPK;
    }
    
}
