/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "USER_APP", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserApp.findAll", query = "SELECT u FROM UserApp u")})
public class UserApp implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserAppPK userAppPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "USER_LOGIN", referencedColumnName = "USER_LOGIN", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AppUser appUser;
    @JoinColumn(name = "APP_ID", referencedColumnName = "APP_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private App app;

    public UserApp() {
    }

    public UserApp(UserAppPK userAppPK) {
        this.userAppPK = userAppPK;
    }

    public UserApp(UserAppPK userAppPK, Boolean actif) {
        this.userAppPK = userAppPK;
        this.actif = actif;
    }

    public UserApp(String userLogin, int appId) {
        this.userAppPK = new UserAppPK(userLogin, appId);
    }

    public UserAppPK getUserAppPK() {
        return userAppPK;
    }

    public void setUserAppPK(UserAppPK userAppPK) {
        this.userAppPK = userAppPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAppPK != null ? userAppPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserApp)) {
            return false;
        }
        UserApp other = (UserApp) object;
        if ((this.userAppPK == null && other.userAppPK != null) || (this.userAppPK != null && !this.userAppPK.equals(other.userAppPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + userAppPK;
    }
    
}
