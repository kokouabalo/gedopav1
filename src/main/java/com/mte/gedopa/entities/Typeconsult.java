/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "TYPECONSULT")
//@SequenceGenerator(name="typeconsult_idtypeconsult_seq", allocationSize = 1)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Typeconsult.findAll", query = "SELECT a FROM Typeconsult a")})
public class Typeconsult implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    @Id
    //@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="typeconsult_idtypeconsult_seq")
    @Basic(optional = false)
    @Column(name = "idtypeconsult")
    private String idtypeconsult;
    @Column(name = "libtype")
    private String libtype;
     
    public Typeconsult() {
    }

    public Typeconsult(String idtypeconsult) {
        this.idtypeconsult = idtypeconsult;
    }

    public Typeconsult(String idtypeconsult, String libtype) {
        this.idtypeconsult = idtypeconsult;
        this.libtype = libtype;
   }


    @Override
    public String toString() {
       return getClass().getSimpleName() + " :: ID = " + getIdtypeconsult();
    }

    /**
     * @return the idtypeconsult
     */
    public String getIdtypeconsult() {
        return idtypeconsult;
    }

    /**
     * @param idtypeconsult the idtypeconsult to set
     */
    public void setIdtypeconsult(String idtypeconsult) {
        this.idtypeconsult = idtypeconsult;
    }

    /**
     * @return the libtype
     */
    public String getLibtype() {
        return libtype;
    }

    /**
     * @param libtype the libtype to set
     */
    public void setLibtype(String libtype) {
        this.libtype = libtype;
    }

    
}
