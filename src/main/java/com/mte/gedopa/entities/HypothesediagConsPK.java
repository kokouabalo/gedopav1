/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author HP
 */
@Embeddable
public class HypothesediagConsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "idhypo")
    private String idhypo;
    @Basic(optional = false)
    @Column(name = "idconsultaion")
    private long idconsultaion;

    public HypothesediagConsPK() {
    }

    public HypothesediagConsPK(String idhypo, long idconsultaion) {
        this.idhypo = idhypo;
        this.idconsultaion = idconsultaion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdhypo() != null ? getIdhypo().hashCode() : 0);
        hash += (int) getIdconsultaion();
        return hash;
    }

    /*@Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HypothesediagConsPK)) {
            return false;
        }
        HypothesediagConsPK other = (HypothesediagConsPK) object;
        if (this.getIdhypo() != other.getIdhypo()) {
            return false;
        }
        if (this.getIdconsultaion() != other.getIdconsultaion()) {
            return false;
        }
        return true;
    }*/
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HypothesediagConsPK)) {
            return false;
        }
        HypothesediagConsPK other = (HypothesediagConsPK) object;
        if ((this.getIdhypo() == null && other.getIdhypo() != null) || (this.getIdhypo() != null && !this.idhypo.equals(other.idhypo))) {
            return false;
        }
        if (this.getIdconsultaion() != other.getIdconsultaion()) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: idhypo = " + getIdhypo() + " :: idconsultaion = " + getIdconsultaion();
    }

    /**
     * @return the idhypo
     */
    public String getIdhypo() {
        return idhypo;
    }

    /**
     * @param idhypo the idhypo to set
     */
    public void setIdhypo(String idhypo) {
        this.idhypo = idhypo;
    }

    /**
     * @return the idconsultaion
     */
    public long getIdconsultaion() {
        return idconsultaion;
    }

    /**
     * @param idconsultaion the idconsultaion to set
     */
    public void setIdconsultaion(long idconsultaion) {
        this.idconsultaion = idconsultaion;
    }
    
}
