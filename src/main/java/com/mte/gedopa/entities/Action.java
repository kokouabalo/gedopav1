/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "ACTION", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Action.findAll", query = "SELECT a FROM Action a")})
public class Action implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(max = 10)
    @Column(name = "ACTION_CODE")
    private String actionCode;
    @Size(max = 30)
    @Column(name = "ACTION_NOM")
    private String actionNom;
    @Size(max = 200)
    @Column(name = "ACTION_DESC")
    private String actionDesc;
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "SHIRO_CODE")
    private String shiroCode;
    @ManyToMany(mappedBy = "actionList")
    private List<Screen> screenList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "actionCode")
    private List<SysLog> sysLogList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "action")
    private List<DroitUser> droitUserList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "action")
    private List<DroitProfil> droitProfilList;

    public Action() {
    }

    public Action(String actionCode) {
        this.actionCode = actionCode;
    }

    public Action(String actionCode, String shiroCode) {
        this.actionCode = actionCode;
        this.shiroCode = shiroCode;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionNom() {
        return actionNom;
    }

    public void setActionNom(String actionNom) {
        this.actionNom = actionNom;
    }

    public String getActionDesc() {
        return actionDesc;
    }

    public void setActionDesc(String actionDesc) {
        this.actionDesc = actionDesc;
    }

    public String getShiroCode() {
        return shiroCode;
    }

    public void setShiroCode(String shiroCode) {
        this.shiroCode = shiroCode;
    }

    @XmlTransient
    public List<Screen> getScreenList() {
        return screenList;
    }

    public void setScreenList(List<Screen> screenList) {
        this.screenList = screenList;
    }

    @XmlTransient
    public List<SysLog> getSysLogList() {
        return sysLogList;
    }

    public void setSysLogList(List<SysLog> sysLogList) {
        this.sysLogList = sysLogList;
    }

    @XmlTransient
    public List<DroitUser> getDroitUserList() {
        return droitUserList;
    }

    public void setDroitUserList(List<DroitUser> droitUserList) {
        this.droitUserList = droitUserList;
    }

    @XmlTransient
    public List<DroitProfil> getDroitProfilList() {
        return droitProfilList;
    }

    public void setDroitProfilList(List<DroitProfil> droitProfilList) {
        this.droitProfilList = droitProfilList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actionCode != null ? actionCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Action)) {
            return false;
        }
        Action other = (Action) object;
        if ((this.actionCode == null && other.actionCode != null) || (this.actionCode != null && !this.actionCode.equals(other.actionCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + actionCode + " :: LIB = " + actionNom;
    }
    
}
