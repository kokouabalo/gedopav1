/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author HP
 */
@Embeddable
public class DroitProfilPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(max = 20)
    @Column(name = "PROFIL_CODE")
    private String profilCode;
    @Basic(optional = false)
    @NotNull
    @Size(max = 10)
    @Column(name = "ACTION_CODE")
    private String actionCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SCREEN_ID")
    private int screenId;

    public DroitProfilPK() {
    }

    public DroitProfilPK(String profilCode, String actionCode, int screenId) {
        this.profilCode = profilCode;
        this.actionCode = actionCode;
        this.screenId = screenId;
    }

    public String getProfilCode() {
        return profilCode;
    }

    public void setProfilCode(String profilCode) {
        this.profilCode = profilCode;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public int getScreenId() {
        return screenId;
    }

    public void setScreenId(int screenId) {
        this.screenId = screenId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profilCode != null ? profilCode.hashCode() : 0);
        hash += (actionCode != null ? actionCode.hashCode() : 0);
        hash += (int) screenId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DroitProfilPK)) {
            return false;
        }
        DroitProfilPK other = (DroitProfilPK) object;
        if ((this.profilCode == null && other.profilCode != null) || (this.profilCode != null && !this.profilCode.equals(other.profilCode))) {
            return false;
        }
        if ((this.actionCode == null && other.actionCode != null) || (this.actionCode != null && !this.actionCode.equals(other.actionCode))) {
            return false;
        }
        if (this.screenId != other.screenId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: profilCode = " + profilCode + " :: actionCode = " + actionCode + " :: screenId = " + screenId;
    }
    
}
