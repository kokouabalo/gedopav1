/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "NOTIF", schema = "PUBLIC")
@SequenceGenerator(name="S_NOTIF", allocationSize = 1, schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notif.findAll", query = "SELECT n FROM Notif n")})
public class Notif implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="S_NOTIF")
    @Basic(optional = false)
    @Column(name = "NOTIF_ID")
    private Long notifId;
    @Basic(optional = false)
    @Column(name = "NOTIF_DATE")
    @Temporal(TemporalType.DATE)
    private Date notifDate;
    @Basic(optional = false)
    @Column(name = "NOTIF_DATE_ECHEANCE")
    @Temporal(TemporalType.DATE)
    private Date notifDateEcheance;
    @Basic(optional = false)
    @Size(max = 60)
    @Column(name = "NOTIF_TITRE")
    private String notifTitre;
    @Basic(optional = false)
    @Size(max = 100)
    @Column(name = "NOTIF_SHORT_DESC")
    private String notifShortDesc;
    @Size(max = 400)
    @Column(name = "NOTIF_DESC")
    private String notifDesc;
    @Basic(optional = false)
    @Size(max = 30)
    @Column(name = "NOTIF_ICON")
    private String notifIcon;
    @Basic(optional = false)
    @Column(name = "MARQUEE")
    private Boolean marquee;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "notif")
    private List<NotifUser> notifUserList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "notif")
    private List<NotifProfil> notifProfilList;
    @JoinColumn(name = "TYPE_NOTIF_CODE", referencedColumnName = "TYPE_NOTIF_CODE")
    @ManyToOne(optional = false)
    private TypeNotif typeNotifCode;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "SCREEN_ID")
    @ManyToOne
    private Screen screenId;

    public Notif() {
    }

    public Notif(Long notifId) {
        this.notifId = notifId;
    }

    public Notif(Long notifId, Date notifDate, Date notifDateEcheance, String notifTitre, String notifShortDesc, String notifIcon, Boolean marquee, Boolean actif) {
        this.notifId = notifId;
        this.notifDate = notifDate;
        this.notifDateEcheance = notifDateEcheance;
        this.notifTitre = notifTitre;
        this.notifShortDesc = notifShortDesc;
        this.notifIcon = notifIcon;
        this.marquee = marquee;
        this.actif = actif;
    }

    public Long getNotifId() {
        return notifId;
    }

    public void setNotifId(Long notifId) {
        this.notifId = notifId;
    }

    public Date getNotifDate() {
        return notifDate;
    }

    public void setNotifDate(Date notifDate) {
        this.notifDate = notifDate;
    }

    public Date getNotifDateEcheance() {
        return notifDateEcheance;
    }

    public void setNotifDateEcheance(Date notifDateEcheance) {
        this.notifDateEcheance = notifDateEcheance;
    }

    public String getNotifTitre() {
        return notifTitre;
    }

    public void setNotifTitre(String notifTitre) {
        this.notifTitre = notifTitre;
    }

    public String getNotifShortDesc() {
        return notifShortDesc;
    }

    public void setNotifShortDesc(String notifShortDesc) {
        this.notifShortDesc = notifShortDesc;
    }

    public String getNotifDesc() {
        return notifDesc;
    }

    public void setNotifDesc(String notifDesc) {
        this.notifDesc = notifDesc;
    }

    public String getNotifIcon() {
        return notifIcon;
    }

    public void setNotifIcon(String notifIcon) {
        this.notifIcon = notifIcon;
    }

    public Boolean getMarquee() {
        return marquee;
    }

    public void setMarquee(Boolean marquee) {
        this.marquee = marquee;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    @XmlTransient
    public List<NotifUser> getNotifUserList() {
        return notifUserList;
    }

    public void setNotifUserList(List<NotifUser> notifUserList) {
        this.notifUserList = notifUserList;
    }

    @XmlTransient
    public List<NotifProfil> getNotifProfilList() {
        return notifProfilList;
    }

    public void setNotifProfilList(List<NotifProfil> notifProfilList) {
        this.notifProfilList = notifProfilList;
    }

    public TypeNotif getTypeNotifCode() {
        return typeNotifCode;
    }

    public void setTypeNotifCode(TypeNotif typeNotifCode) {
        this.typeNotifCode = typeNotifCode;
    }

    public Screen getScreenId() {
        return screenId;
    }

    public void setScreenId(Screen screenId) {
        this.screenId = screenId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notifId != null ? notifId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notif)) {
            return false;
        }
        Notif other = (Notif) object;
        if ((this.notifId == null && other.notifId != null) || (this.notifId != null && !this.notifId.equals(other.notifId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + notifId;
    }
    
}
