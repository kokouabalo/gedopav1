/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "PROFIL_REPORT", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProfilReport.findAll", query = "SELECT p FROM ProfilReport p")})
public class ProfilReport implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProfilReportPK profilReportPK;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "REPORT_ID", referencedColumnName = "REPORT_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Report report;
    @JoinColumn(name = "PROFIL_CODE", referencedColumnName = "PROFIL_CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Profil profil;

    public ProfilReport() {
    }

    public ProfilReport(ProfilReportPK profilReportPK) {
        this.profilReportPK = profilReportPK;
    }

    public ProfilReport(ProfilReportPK profilReportPK, Boolean actif) {
        this.profilReportPK = profilReportPK;
        this.actif = actif;
    }

    public ProfilReport(String profilCode, int reportId) {
        this.profilReportPK = new ProfilReportPK(profilCode, reportId);
    }

    public ProfilReportPK getProfilReportPK() {
        return profilReportPK;
    }

    public void setProfilReportPK(ProfilReportPK profilReportPK) {
        this.profilReportPK = profilReportPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profilReportPK != null ? profilReportPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfilReport)) {
            return false;
        }
        ProfilReport other = (ProfilReport) object;
        if ((this.profilReportPK == null && other.profilReportPK != null) || (this.profilReportPK != null && !this.profilReportPK.equals(other.profilReportPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + profilReportPK;
    }
    
}
