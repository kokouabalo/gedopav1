/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "SPECIALITE")
@SequenceGenerator(name="specialite_idspec_seq", allocationSize = 1)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Specialite.findAll", query = "SELECT a FROM Specialite a")})
public class Specialite implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="specialite_idspec_seq")
    @Basic(optional = false)
    @Column(name = "idspec")
    private Integer idspec;
    @Column(name = "libspec")
    private String libspec;
     
    public Specialite() {
    }

    public Specialite(Integer idspec) {
        this.idspec = idspec;
    }

    public Specialite(Integer idspec, String libspec) {
        this.idspec = idspec;
        this.libspec = libspec;
   }


    @Override
    public String toString() {
       return getClass().getSimpleName() + " :: ID = " + getIdspec();
    }

    /**
     * @return the idspec
     */
    public Integer getIdspec() {
        return idspec;
    }

    /**
     * @param idspec the idspec to set
     */
    public void setIdspec(Integer idspec) {
        this.idspec = idspec;
    }

    /**
     * @return the libspec
     */
    public String getLibspec() {
        return libspec;
    }

    /**
     * @param libspec the libspec to set
     */
    public void setLibspec(String libspec) {
        this.libspec = libspec;
    }
   
}
