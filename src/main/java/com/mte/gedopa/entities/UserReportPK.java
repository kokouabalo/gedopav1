/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author HP
 */
@Embeddable
public class UserReportPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "REPORT_ID")
    private int reportId;
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "USER_LOGIN")
    private String userLogin;

    public UserReportPK() {
    }

    public UserReportPK(int reportId, String userLogin) {
        this.reportId = reportId;
        this.userLogin = userLogin;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) reportId;
        hash += (userLogin != null ? userLogin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserReportPK)) {
            return false;
        }
        UserReportPK other = (UserReportPK) object;
        if (this.reportId != other.reportId) {
            return false;
        }
        if ((this.userLogin == null && other.userLogin != null) || (this.userLogin != null && !this.userLogin.equals(other.userLogin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: reportId = " + reportId + " :: userLogin = " + userLogin;
    }
    
}
