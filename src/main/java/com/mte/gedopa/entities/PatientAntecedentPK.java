/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author HP
 */
@Embeddable
public class PatientAntecedentPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "idantecedent")
    private int idantecedent;
    @Basic(optional = false)
    @Column(name = "id_patient")
    private long id_patient;

    public PatientAntecedentPK() {
    }

    public PatientAntecedentPK(int idantecedent, long id_patient) {
        this.idantecedent = idantecedent;
        this.id_patient = id_patient;
    }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) getIdantecedent();
        hash += (int) getId_patient();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PatientAntecedentPK)) {
            return false;
        }
        PatientAntecedentPK other = (PatientAntecedentPK) object;
        if (this.getIdantecedent() != other.getIdantecedent()) {
            return false;
        }
        if (this.getId_patient() != other.getId_patient()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: idantecedent = " + getIdantecedent() + " :: id_patient = " + getId_patient();
    }

    /**
     * @return the idantecedent
     */
    public int getIdantecedent() {
        return idantecedent;
    }

    /**
     * @param idantecedent the idantecedent to set
     */
    public void setIdantecedent(int idantecedent) {
        this.idantecedent = idantecedent;
    }

    /**
     * @return the id_patient
     */
    public long getId_patient() {
        return id_patient;
    }

    /**
     * @param id_patient the id_patient to set
     */
    public void setId_patient(long id_patient) {
        this.id_patient = id_patient;
    }
    
}
