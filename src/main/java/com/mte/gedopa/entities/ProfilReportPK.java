/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author HP
 */
@Embeddable
public class ProfilReportPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(max = 20)
    @Column(name = "PROFIL_CODE")
    private String profilCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REPORT_ID")
    private int reportId;

    public ProfilReportPK() {
    }

    public ProfilReportPK(String profilCode, int reportId) {
        this.profilCode = profilCode;
        this.reportId = reportId;
    }

    public String getProfilCode() {
        return profilCode;
    }

    public void setProfilCode(String profilCode) {
        this.profilCode = profilCode;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profilCode != null ? profilCode.hashCode() : 0);
        hash += (int) reportId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfilReportPK)) {
            return false;
        }
        ProfilReportPK other = (ProfilReportPK) object;
        if ((this.profilCode == null && other.profilCode != null) || (this.profilCode != null && !this.profilCode.equals(other.profilCode))) {
            return false;
        }
        if (this.reportId != other.reportId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: profilCode=" + profilCode + " :: reportId=" + reportId;
    }
    
}
