/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "CONSULTATION")
@SequenceGenerator(name="consultation_idconsultaion_seq", allocationSize = 1)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultation.findAll", query = "SELECT a FROM Consultation a")})
public class Consultation implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="consultation_idconsultaion_seq")
    @Basic(optional = false)
    @Column(name = "idconsultaion")
    private Long idconsultaion;
    @Column(name = "dateconsultation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateconsultation;
    @Column(name = "daterendezvous")
    @Temporal(TemporalType.TIMESTAMP)
    private Date daterendezvous;
    @Lob
    @Column(name = "descconsultation")
    private String descconsultation;
    @Column(name = "poidspatient")
    private Double poidspatient;
    @Column(name = "tensionpatient")
    private Double tensionpatient;
    @Column(name = "temppatient")
    private Double temppatient;
    
    @JoinColumn(name = "idtypeconsult", referencedColumnName = "idtypeconsult")
    @ManyToOne(optional = false)
    private Typeconsult idtypeconsult;
    
    @JoinColumn(name = "id_patient", referencedColumnName = "id_patient")
    @ManyToOne(optional = false)
    private Patient id_patient;
    
    @JoinColumn(name = "idmed", referencedColumnName = "idmed")
    @ManyToOne(optional = false)
    private Medecin idmed;
    
    
//    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
//    private List<DeclarCodeIsps> declarCodeIspsList;
//    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
//    private List<DeclMaritimeSante> declMaritimeSanteList;
//    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "annonceEscale")
//    private List<OperationEscale> operationEscaleList;
//    @JoinColumn(name = "ID_TYPE_ESCALE", referencedColumnName = "ID_TYPE_ESCALE")
//    @ManyToOne(optional = false)
//    private TypeEscale idTypeEscale;
//    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
//    private List<Sejour> sejourList;
    
 /*   @JoinColumn(name = "CODE_SITU_ESCALE", referencedColumnName = "CODE_SITU_ESCALE")
    @ManyToOne(optional = false)
    private SituationEscale codeSituEscale;
    
    @JoinColumn(name = "POSTE_PREVU", referencedColumnName = "CODE_POSTE")
    @ManyToOne(optional = false)
    private Poste postePrevu;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<PassagerVue> passagerVueList;
    @OneToMany(mappedBy = "idAnnonceEscale")
    private List<Reserve> reserveList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "idAnnonceEscale")
    private List<RapportEscale> rapportEscaleList; */
  
    public Consultation() {
    }

    public Consultation(Long idconsultaion) {
        this.idconsultaion = idconsultaion;
    }

    public Consultation(Long idconsultaion, Date dateconsultation, Date daterendezvous) {
        this.idconsultaion = idconsultaion;
        this.dateconsultation = dateconsultation;
        this.daterendezvous = daterendezvous;
    }


    @Override
    public String toString() {
       return getClass().getSimpleName() + " :: ID = " + getIdconsultaion();
    }

    /**
     * @return the idconsultaion
     */
    public Long getIdconsultaion() {
        return idconsultaion;
    }

    /**
     * @param idconsultaion the idconsultaion to set
     */
    public void setIdconsultaion(Long idconsultaion) {
        this.idconsultaion = idconsultaion;
    }

    /**
     * @return the dateconsultation
     */
    public Date getDateconsultation() {
        return dateconsultation;
    }

    /**
     * @param dateconsultation the dateconsultation to set
     */
    public void setDateconsultation(Date dateconsultation) {
        this.dateconsultation = dateconsultation;
    }

    /**
     * @return the daterendezvous
     */
    public Date getDaterendezvous() {
        return daterendezvous;
    }

    /**
     * @param daterendezvous the daterendezvous to set
     */
    public void setDaterendezvous(Date daterendezvous) {
        this.daterendezvous = daterendezvous;
    }

    /**
     * @return the descconsultation
     */
    public String getDescconsultation() {
        return descconsultation;
    }

    /**
     * @param descconsultation the descconsultation to set
     */
    public void setDescconsultation(String descconsultation) {
        this.descconsultation = descconsultation;
    }

    /**
     * @return the poidspatient
     */
    public Double getPoidspatient() {
        return poidspatient;
    }

    /**
     * @param poidspatient the poidspatient to set
     */
    public void setPoidspatient(Double poidspatient) {
        this.poidspatient = poidspatient;
    }

    /**
     * @return the tensionpatient
     */
    public Double getTensionpatient() {
        return tensionpatient;
    }

    /**
     * @param tensionpatient the tensionpatient to set
     */
    public void setTensionpatient(Double tensionpatient) {
        this.tensionpatient = tensionpatient;
    }

    /**
     * @return the temppatient
     */
    public Double getTemppatient() {
        return temppatient;
    }

    /**
     * @param temppatient the temppatient to set
     */
    public void setTemppatient(Double temppatient) {
        this.temppatient = temppatient;
    }

    /**
     * @return the idtypeconsult
     */
    public Typeconsult getIdtypeconsult() {
        return idtypeconsult;
    }

    /**
     * @param idtypeconsult the idtypeconsult to set
     */
    public void setIdtypeconsult(Typeconsult idtypeconsult) {
        this.idtypeconsult = idtypeconsult;
    }

    /**
     * @return the id_patient
     */
    public Patient getId_patient() {
        return id_patient;
    }

    /**
     * @param id_patient the id_patient to set
     */
    public void setId_patient(Patient id_patient) {
        this.id_patient = id_patient;
    }

    /**
     * @return the idmed
     */
    public Medecin getIdmed() {
        return idmed;
    }

    /**
     * @param idmed the idmed to set
     */
    public void setIdmed(Medecin idmed) {
        this.idmed = idmed;
    }

    
}
