/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "REPORT_PARAM", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportParam.findAll", query = "SELECT r FROM ReportParam r")})
public class ReportParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReportParamPK reportParamPK;
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;
    @JoinColumn(name = "REPORT_ID", referencedColumnName = "REPORT_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Report report;
    @JoinColumn(name = "PARAM_ID", referencedColumnName = "PARAM_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Param param;

    public ReportParam() {
    }

    public ReportParam(ReportParamPK reportParamPK) {
        this.reportParamPK = reportParamPK;
    }

    public ReportParam(ReportParamPK reportParamPK, String defaultValue) {
        this.reportParamPK = reportParamPK;
        this.defaultValue = defaultValue;
    }

    public ReportParam(int reportId, int paramId) {
        this.reportParamPK = new ReportParamPK(reportId, paramId);
    }

    public ReportParamPK getReportParamPK() {
        return reportParamPK;
    }

    public void setReportParamPK(ReportParamPK reportParamPK) {
        this.reportParamPK = reportParamPK;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportParamPK != null ? reportParamPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportParam)) {
            return false;
        }
        ReportParam other = (ReportParam) object;
        if ((this.reportParamPK == null && other.reportParamPK != null) || (this.reportParamPK != null && !this.reportParamPK.equals(other.reportParamPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = "  + reportParamPK;
    }
    
}
