/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "SYS_LOG", schema = "PUBLIC")
@SequenceGenerator(name="S_SYS_LOG", allocationSize = 1, schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SysLog.findAll", query = "SELECT s FROM SysLog s")})
public class SysLog implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="S_SYS_LOG")
    @Basic(optional = false)
    @Column(name = "LOG_ID")
    private BigDecimal logId;
    @Basic(optional = false)
    @Column(name = "LOG_DESC")
    private String logDesc;
    @Basic(optional = false)
    @Column(name = "LOG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logDate;
    @Column(name = "ENTITY")
    private String entity;
    @Column(name = "ENTITY_ID")
    private String entityId;
    @JoinColumn(name = "USER_LOGIN", referencedColumnName = "USER_LOGIN")
    @ManyToOne(optional = false)
    private AppUser userLogin;
    @JoinColumn(name = "ACTION_CODE", referencedColumnName = "ACTION_CODE")
    @ManyToOne(optional = false)
    private Action actionCode;

    public SysLog() {
    }

    public SysLog(BigDecimal logId) {
        this.logId = logId;
    }

    public SysLog(BigDecimal logId, String logDesc, Date logDate) {
        this.logId = logId;
        this.logDesc = logDesc;
        this.logDate = logDate;
    }

    public BigDecimal getLogId() {
        return logId;
    }

    public void setLogId(BigDecimal logId) {
        this.logId = logId;
    }

    public String getLogDesc() {
        return logDesc;
    }

    public void setLogDesc(String logDesc) {
        this.logDesc = logDesc;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public AppUser getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(AppUser userLogin) {
        this.userLogin = userLogin;
    }

    public Action getActionCode() {
        return actionCode;
    }

    public void setActionCode(Action actionCode) {
        this.actionCode = actionCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logId != null ? logId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysLog)) {
            return false;
        }
        SysLog other = (SysLog) object;
        if ((this.logId == null && other.logId != null) || (this.logId != null && !this.logId.equals(other.logId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + logId;
    }
    
}
