/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author HP
 */
@Embeddable
public class UserAppPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(max = 100)
    @Column(name = "USER_LOGIN")
    private String userLogin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APP_ID")
    private int appId;

    public UserAppPK() {
    }

    public UserAppPK(String userLogin, int appId) {
        this.userLogin = userLogin;
        this.appId = appId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userLogin != null ? userLogin.hashCode() : 0);
        hash += (int) appId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAppPK)) {
            return false;
        }
        UserAppPK other = (UserAppPK) object;
        if ((this.userLogin == null && other.userLogin != null) || (this.userLogin != null && !this.userLogin.equals(other.userLogin))) {
            return false;
        }
        if (this.appId != other.appId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: userLogin = " + userLogin + " :: appId = " + appId;
    }
    
}
