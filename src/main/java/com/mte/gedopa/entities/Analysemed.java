/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "ANALYSEMED")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Analysemed.findAll", query = "SELECT a FROM Analysemed a")})
public class Analysemed implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idanamed")
    private String idanamed;
    @Column(name = "designationanamed")
    private String designationanamed;
     
    public Analysemed() {
    }

    public Analysemed(String idanamed) {
        this.idanamed = idanamed;
    }

    public Analysemed(String idanamed, String designationanamed) {
        this.idanamed = idanamed;
        this.designationanamed = designationanamed;
   }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getIdanamed()!= null ? getIdanamed().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Analysemed)) {
            return false;
        }
        Analysemed other = (Analysemed) object;
        if ((this.getIdanamed() == null && other.getIdanamed() != null) || (this.getIdanamed() != null && !this.idanamed.equals(other.idanamed))) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + getIdanamed() + " :: LIB = " + getDesignationanamed();
    }

    /**
     * @return the idanamed
     */
    public String getIdanamed() {
        return idanamed;
    }

    /**
     * @param idanamed the idanamed to set
     */
    public void setIdanamed(String idanamed) {
        this.idanamed = idanamed;
    }

    /**
     * @return the designationanamed
     */
    public String getDesignationanamed() {
        return designationanamed;
    }

    /**
     * @param designationanamed the designationanamed to set
     */
    public void setDesignationanamed(String designationanamed) {
        this.designationanamed = designationanamed;
    }
}
