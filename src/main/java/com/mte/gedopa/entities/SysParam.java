/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "SYS_PARAM", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SysParam.findAll", query = "SELECT s FROM SysParam s")})
public class SysParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(max = 60)
    @Column(name = "SYS_PARAM_CODE")
    private String sysParamCode;
    @Size(max = 200)
    @Column(name = "SYS_PARAM_DESC")
    private String sysParamDesc;
    @Size(max = 500)
    @Column(name = "SYS_PARAM_VALUE")
    private String sysParamValue;
    @Size(max = 30)
    @Column(name = "FILTER_VALUE")
    private String filterValue;

    public SysParam() {
    }

    public SysParam(String sysParamCode) {
        this.sysParamCode = sysParamCode;
    }

    public String getSysParamCode() {
        return sysParamCode;
    }

    public void setSysParamCode(String sysParamCode) {
        this.sysParamCode = sysParamCode;
    }

    public String getSysParamDesc() {
        return sysParamDesc;
    }

    public void setSysParamDesc(String sysParamDesc) {
        this.sysParamDesc = sysParamDesc;
    }

    public String getSysParamValue() {
        return sysParamValue;
    }

    public void setSysParamValue(String sysParamValue) {
        this.sysParamValue = sysParamValue;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sysParamCode != null ? sysParamCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysParam)) {
            return false;
        }
        SysParam other = (SysParam) object;
        if ((this.sysParamCode == null && other.sysParamCode != null) || (this.sysParamCode != null && !this.sysParamCode.equals(other.sysParamCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + sysParamCode;
    }
    
}
