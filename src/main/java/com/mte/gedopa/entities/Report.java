/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "REPORT", schema = "PUBLIC")
@SequenceGenerator(name="S_REPORT", allocationSize = 1, schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Report.findAll", query = "SELECT r FROM Report r")})
public class Report implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="S_REPORT")
    @Basic(optional = false)
    @Column(name = "REPORT_ID")
    private Integer reportId;
    @Basic(optional = false)
    @Size(max = 100)
    @Column(name = "REPORT_FILE")
    private String reportFile;
    @Size(max = 200)
    @Column(name = "REPORT_NAME")
    private String reportName;
    @Basic(optional = false)
    @Column(name = "NUM_ORDRE")
    private BigInteger numOrdre;
    @JoinColumn(name = "REP_TYPE_CODE", referencedColumnName = "REP_TYPE_CODE")
    @ManyToOne(optional = false)
    private ReportType repTypeCode;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "report")
    private List<UserReport> userReportList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "report")
    private List<ReportParam> reportParamList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "report")
    private List<ProfilReport> profilReportList;

    public Report() {
    }

    public Report(Integer reportId) {
        this.reportId = reportId;
    }

    public Report(Integer reportId, String reportFile, BigInteger numOrdre) {
        this.reportId = reportId;
        this.reportFile = reportFile;
        this.numOrdre = numOrdre;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public BigInteger getNumOrdre() {
        return numOrdre;
    }

    public void setNumOrdre(BigInteger numOrdre) {
        this.numOrdre = numOrdre;
    }

    public ReportType getRepTypeCode() {
        return repTypeCode;
    }

    public void setRepTypeCode(ReportType repTypeCode) {
        this.repTypeCode = repTypeCode;
    }

    @XmlTransient
    public List<UserReport> getUserReportList() {
        return userReportList;
    }

    public void setUserReportList(List<UserReport> userReportList) {
        this.userReportList = userReportList;
    }

    @XmlTransient
    public List<ReportParam> getReportParamList() {
        return reportParamList;
    }

    public void setReportParamList(List<ReportParam> reportParamList) {
        this.reportParamList = reportParamList;
    }

    @XmlTransient
    public List<ProfilReport> getProfilReportList() {
        return profilReportList;
    }

    public void setProfilReportList(List<ProfilReport> profilReportList) {
        this.profilReportList = profilReportList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportId != null ? reportId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Report)) {
            return false;
        }
        Report other = (Report) object;
        if ((this.reportId == null && other.reportId != null) || (this.reportId != null && !this.reportId.equals(other.reportId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + reportId + " :: LIB = " + reportName;
    }
    
}
