/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "DROIT_USER", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DroitUser.findAll", query = "SELECT d FROM DroitUser d")})
public class DroitUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DroitUserPK droitUserPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "SCREEN_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Screen screen;
    @JoinColumn(name = "USER_LOGIN", referencedColumnName = "USER_LOGIN", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AppUser appUser;
    @JoinColumn(name = "ACTION_CODE", referencedColumnName = "ACTION_CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Action action;

    public DroitUser() {
    }

    public DroitUser(DroitUserPK droitUserPK) {
        this.droitUserPK = droitUserPK;
    }

    public DroitUser(DroitUserPK droitUserPK, Boolean actif) {
        this.droitUserPK = droitUserPK;
        this.actif = actif;
    }

    public DroitUser(String actionCode, String userLogin, int screenId) {
        this.droitUserPK = new DroitUserPK(actionCode, userLogin, screenId);
    }

    public DroitUserPK getDroitUserPK() {
        return droitUserPK;
    }

    public void setDroitUserPK(DroitUserPK droitUserPK) {
        this.droitUserPK = droitUserPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (droitUserPK != null ? droitUserPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DroitUser)) {
            return false;
        }
        DroitUser other = (DroitUser) object;
        if ((this.droitUserPK == null && other.droitUserPK != null) || (this.droitUserPK != null && !this.droitUserPK.equals(other.droitUserPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + droitUserPK;
    }
    
}
