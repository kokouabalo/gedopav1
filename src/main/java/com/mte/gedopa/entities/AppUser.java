/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "APP_USER", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AppUser.findAll", query = "SELECT a FROM AppUser a")})
public class AppUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_LOGIN")
    private String userLogin;
    @Basic(optional = false)
    @Column(name = "USER_PASSWORD")
    private String userPassword;
    @Size(max = 100)
    @Column(name = "USER_NOM")
    private String userNom;    
    @Size(max = 150)
    @Column(name = "USER_EMAIL")
    private String userEmail;
    @Size(max = 30)
    @Column(name = "USER_PHOTO")
    private String userPhoto;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @Basic(optional = false)
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.DATE)
    private Date dateCreation;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "appUser")
    private List<NotifUser> notifUserList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "appUser")
    private List<UserReport> userReportList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "appUser")
    private List<UserApp> userAppList;
    @JoinColumn(name = "PROFIL_CODE", referencedColumnName = "PROFIL_CODE")
    @ManyToOne(optional = false)
    private Profil profilCode;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "appUser")
    private List<UserMenu> userMenuList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "userLogin")
    private List<SysLog> sysLogList;
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "appUser")
    private List<DroitUser> droitUserList;

    public AppUser() {
    }

    public AppUser(String userLogin) {
        this.userLogin = userLogin;
    }

    public AppUser(String userLogin, String userPassword, Boolean actif) {
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.actif = actif;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserNom() {
        return userNom;
    }

    public void setUserNom(String userNom) {
        this.userNom = userNom;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    @XmlTransient
    public List<NotifUser> getNotifUserList() {
        return notifUserList;
    }

    public void setNotifUserList(List<NotifUser> notifUserList) {
        this.notifUserList = notifUserList;
    }

    @XmlTransient
    public List<UserReport> getUserReportList() {
        return userReportList;
    }

    public void setUserReportList(List<UserReport> userReportList) {
        this.userReportList = userReportList;
    }

    @XmlTransient
    public List<UserApp> getUserAppList() {
        return userAppList;
    }

    public void setUserAppList(List<UserApp> userAppList) {
        this.userAppList = userAppList;
    }

    public Profil getProfilCode() {
        return profilCode;
    }

    public void setProfilCode(Profil profilCode) {
        this.profilCode = profilCode;
    }

    @XmlTransient
    public List<UserMenu> getUserMenuList() {
        return userMenuList;
    }

    public void setUserMenuList(List<UserMenu> userMenuList) {
        this.userMenuList = userMenuList;
    }

    @XmlTransient
    public List<SysLog> getSysLogList() {
        return sysLogList;
    }

    public void setSysLogList(List<SysLog> sysLogList) {
        this.sysLogList = sysLogList;
    }

    @XmlTransient
    public List<DroitUser> getDroitUserList() {
        return droitUserList;
    }

    public void setDroitUserList(List<DroitUser> droitUserList) {
        this.droitUserList = droitUserList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userLogin != null ? userLogin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppUser)) {
            return false;
        }
        AppUser other = (AppUser) object;
        if ((this.userLogin == null && other.userLogin != null) || (this.userLogin != null && !this.userLogin.equals(other.userLogin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + userLogin;
    }
    
}
