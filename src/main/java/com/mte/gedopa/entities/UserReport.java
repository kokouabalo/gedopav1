/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "USER_REPORT", schema = "PUBLIC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserReport.findAll", query = "SELECT u FROM UserReport u")})
public class UserReport implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserReportPK userReportPK;
    @Basic(optional = false)
    @Column(name = "ACTIF")
    private Boolean actif;
    @JoinColumn(name = "REPORT_ID", referencedColumnName = "REPORT_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Report report;
    @JoinColumn(name = "USER_LOGIN", referencedColumnName = "USER_LOGIN", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AppUser appUser;

    public UserReport() {
    }

    public UserReport(UserReportPK userReportPK) {
        this.userReportPK = userReportPK;
    }

    public UserReport(UserReportPK userReportPK, Boolean actif) {
        this.userReportPK = userReportPK;
        this.actif = actif;
    }

    public UserReport(int reportId, String userLogin) {
        this.userReportPK = new UserReportPK(reportId, userLogin);
    }

    public UserReportPK getUserReportPK() {
        return userReportPK;
    }

    public void setUserReportPK(UserReportPK userReportPK) {
        this.userReportPK = userReportPK;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userReportPK != null ? userReportPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserReport)) {
            return false;
        }
        UserReport other = (UserReport) object;
        if ((this.userReportPK == null && other.userReportPK != null) || (this.userReportPK != null && !this.userReportPK.equals(other.userReportPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + userReportPK;
    }
    
}
