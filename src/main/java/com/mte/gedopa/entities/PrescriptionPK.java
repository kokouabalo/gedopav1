/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author HP
 */
@Embeddable
public class PrescriptionPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "codemedi")
    private String codemedi;
    @Basic(optional = false)
    @Column(name = "idconsultaion")
    private long idconsultaion;

    public PrescriptionPK() {
    }

    public PrescriptionPK(String codemedi, long idconsultaion) {
        this.codemedi = codemedi;
        this.idconsultaion = idconsultaion;
    }
    
   @Override
    public int hashCode() {
        int hash = 0;
        hash += (getCodemedi() != null ? getCodemedi().hashCode() : 0);
        hash += (int) getIdconsultaion();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrescriptionPK)) {
            return false;
        }
        PrescriptionPK other = (PrescriptionPK) object;
        if ((this.getCodemedi() == null && other.getCodemedi() != null) || (this.getCodemedi() != null && !this.codemedi.equals(other.codemedi))) {
            return false;
        }
        if (this.getIdconsultaion() != other.getIdconsultaion()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: codemedi = " + getCodemedi() + " :: idconsultaion = " + getIdconsultaion();
    }

    /**
     * @return the codemedi
     */
    public String getCodemedi() {
        return codemedi;
    }

    /**
     * @param codemedi the codemedi to set
     */
    public void setCodemedi(String codemedi) {
        this.codemedi = codemedi;
    }

    /**
     * @return the idconsultaion
     */
    public long getIdconsultaion() {
        return idconsultaion;
    }

    /**
     * @param idconsultaion the idconsultaion to set
     */
    public void setIdconsultaion(long idconsultaion) {
        this.idconsultaion = idconsultaion;
    }
}
