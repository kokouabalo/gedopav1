/*
Medicament
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "MEDICAMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medicament.findAll", query = "SELECT c FROM Medicament c")})
public class Medicament implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "codemedi")
    private String codemedi;
    @Basic(optional = false)
    @Column(name = "libmedi")
    private String libmedi;
 
    public Medicament() {
    }

    public Medicament(String codemedi) {
        this.codemedi = codemedi;
    }

    public Medicament(String codemedi, String libmedi) {
        this.codemedi = codemedi;
        this.libmedi = libmedi;
    }  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getCodemedi() != null ? getCodemedi().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medicament)) {
            return false;
        }
        Medicament other = (Medicament) object;
        if ((this.getCodemedi() == null && other.getCodemedi() != null) || (this.getCodemedi() != null && !this.codemedi.equals(other.codemedi))) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return getClass().getSimpleName() + " :: ID = " + getCodemedi() + " :: LIB = " + getLibmedi();
    }

    /**
     * @return the codemedi
     */
    public String getCodemedi() {
        return codemedi;
    }

    /**
     * @param codemedi the codemedi to set
     */
    public void setCodemedi(String codemedi) {
        this.codemedi = codemedi;
    }

    /**
     * @return the libmedi
     */
    public String getLibmedi() {
        return libmedi;
    }

    /**
     * @param libmedi the libmedi to set
     */
    public void setLibmedi(String libmedi) {
        this.libmedi = libmedi;
    }
}
