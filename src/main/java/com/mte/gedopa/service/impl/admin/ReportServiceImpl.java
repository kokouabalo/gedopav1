
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Report;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ReportService;


/**
 *
 * @author H
 */
public class ReportServiceImpl extends ServiceImpl<Report, Integer> implements ReportService{
    
    private static ReportServiceImpl instance;
    
    private ReportServiceImpl(){
        
    }
    
    public static ReportServiceImpl getInstance(){
        if(instance == null){
            instance = new ReportServiceImpl();
        }
        return instance;
    }
}