
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Action;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ActionService;


/**
 *
 * @author H
 */
public class ActionServiceImpl extends ServiceImpl<Action, String> implements ActionService{
    
    private static ActionServiceImpl instance;
    
    private ActionServiceImpl(){
        
    }
    
    public static ActionServiceImpl getInstance(){
        if(instance == null){
            instance = new ActionServiceImpl();
        }
        return instance;
    }
}