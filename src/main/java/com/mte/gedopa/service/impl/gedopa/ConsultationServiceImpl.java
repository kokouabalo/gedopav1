
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Consultation;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.ConsultationService;

/**
 *
 * @author H
 */
public class ConsultationServiceImpl extends ServiceImpl<Consultation, Long> implements ConsultationService{
    
    private static ConsultationServiceImpl instance;
    
    private ConsultationServiceImpl(){
        
    }
    
    public static ConsultationServiceImpl getInstance(){
        if(instance == null){
            instance = new ConsultationServiceImpl();
        }
        return instance;
    }
}