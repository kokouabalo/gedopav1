
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.App;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.AppService;


/**
 *
 * @author H
 */
public class AppServiceImpl extends ServiceImpl<App, Integer> implements AppService{
    
    private static AppServiceImpl instance;
    
    private AppServiceImpl(){
        
    }
    
    public static AppServiceImpl getInstance(){
        if(instance == null){
            instance = new AppServiceImpl();
        }
        return instance;
    }
}