
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Analysemed;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.AnalysemedService;


/**
 *
 * @author H
 */
public class AnalysemedServiceImpl extends ServiceImpl<Analysemed, String> implements AnalysemedService{
    
    private static AnalysemedServiceImpl instance;
    
    private AnalysemedServiceImpl(){
        
    }
    
    public static AnalysemedServiceImpl getInstance(){
        if(instance == null){
            instance = new AnalysemedServiceImpl();
        }
        return instance;
    }
}