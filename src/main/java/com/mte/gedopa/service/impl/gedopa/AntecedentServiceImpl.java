
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Antecedent;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.AntecedentService;


/**
 *
 * @author H
 */
public class AntecedentServiceImpl extends ServiceImpl<Antecedent, Integer> implements AntecedentService{
    
    private static AntecedentServiceImpl instance;
    
    private AntecedentServiceImpl(){
        
    }
    
    public static AntecedentServiceImpl getInstance(){
        if(instance == null){
            instance = new AntecedentServiceImpl();
        }
        return instance;
    }
}