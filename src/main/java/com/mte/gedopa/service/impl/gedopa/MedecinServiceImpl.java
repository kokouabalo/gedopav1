
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Medecin;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.MedecinService;


/**
 *
 * @author H
 */
public class MedecinServiceImpl extends ServiceImpl<Medecin, String> implements MedecinService{
    
    private static MedecinServiceImpl instance;
    
    private MedecinServiceImpl(){
        
    }
    
    public static MedecinServiceImpl getInstance(){
        if(instance == null){
            instance = new MedecinServiceImpl();
        }
        return instance;
    }
}