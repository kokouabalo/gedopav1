
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Report;
import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.entities.ProfilReport;
import com.mte.gedopa.entities.ProfilReport;
import com.mte.gedopa.entities.ProfilReportPK;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ProfilReportService;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author H
 */
public class ProfilReportServiceImpl extends ServiceImpl<ProfilReport, ProfilReportPK> implements ProfilReportService{
    
    private static ProfilReportServiceImpl instance;
    
    private ProfilReportServiceImpl(){
        
    }
    
    public static ProfilReportServiceImpl getInstance(){
        if(instance == null){
            instance = new ProfilReportServiceImpl();
        }
        return instance;
    }

    @Override
    public List<Report> findByProfil(Profil profil, boolean actifOnly) {
        List<Report> list = new ArrayList<>();
        if(profil == null){
            return list;
        }
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("profil.profilCode", OperateurCond.EQUAL, profil.getProfilCode()));
        if(actifOnly){
            criteres.add(new Critere("actif", OperateurCond.EQUAL, true));
        }
        try {
            List<ProfilReport> profilReports = findBy(criteres);
            if(profilReports != null && !profilReports.isEmpty()){
                for (ProfilReport profilReport : profilReports) {
                    list.add(profilReport.getReport());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ProfilReportServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}