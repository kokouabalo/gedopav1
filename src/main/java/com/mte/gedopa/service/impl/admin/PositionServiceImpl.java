
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Position;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.PositionService;


/**
 *
 * @author H
 */
public class PositionServiceImpl extends ServiceImpl<Position, String> implements PositionService{
    
    private static PositionServiceImpl instance;
    
    private PositionServiceImpl(){
        
    }
    
    public static PositionServiceImpl getInstance(){
        if(instance == null){
            instance = new PositionServiceImpl();
        }
        return instance;
    }
}