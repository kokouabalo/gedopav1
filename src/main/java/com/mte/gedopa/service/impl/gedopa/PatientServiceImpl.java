package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.PatientService;
import com.mte.gedopa.util.Critere;
import java.util.ArrayList;

import java.util.List;

/**
 *
 * @author H
 */
public class PatientServiceImpl extends ServiceImpl<Patient, Long> implements PatientService {

    private static PatientServiceImpl instance;

    private PatientServiceImpl() {

    }

    public static PatientServiceImpl getInstance() {
        if (instance == null) {
            instance = new PatientServiceImpl();
        }
        return instance;
    }

    @Override
    public Patient findByCode(String code) throws Exception {
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("numdossier", code));
        List<Patient> list = findBy(criteres);
        return list.isEmpty() ? null : list.get(0);
    }
}
