
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ProfilService;


/**
 *
 * @author H
 */
public class ProfilServiceImpl extends ServiceImpl<Profil, String> implements ProfilService{
    
    private static ProfilServiceImpl instance;
    
    private ProfilServiceImpl(){
        
    }
    
    public static ProfilServiceImpl getInstance(){
        if(instance == null){
            instance = new ProfilServiceImpl();
        }
        return instance;
    }
}