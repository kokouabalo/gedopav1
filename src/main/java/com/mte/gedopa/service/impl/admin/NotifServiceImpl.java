
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Notif;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.NotifService;


/**
 *
 * @author H
 */
public class NotifServiceImpl extends ServiceImpl<Notif, Integer> implements NotifService{
    
    private static NotifServiceImpl instance;
    
    private NotifServiceImpl(){
        
    }
    
    public static NotifServiceImpl getInstance(){
        if(instance == null){
            instance = new NotifServiceImpl();
        }
        return instance;
    }
}