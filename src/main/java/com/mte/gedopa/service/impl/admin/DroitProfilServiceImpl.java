
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.DroitProfil;
import com.mte.gedopa.entities.DroitProfilPK;
import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.entities.ProfilMenu;
import com.mte.gedopa.entities.Screen;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.DroitProfilService;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author H
 */
public class DroitProfilServiceImpl extends ServiceImpl<DroitProfil, DroitProfilPK> implements DroitProfilService{
    
    private static DroitProfilServiceImpl instance;
    
    private DroitProfilServiceImpl(){
        
    }
    
    public static DroitProfilServiceImpl getInstance(){
        if(instance == null){
            instance = new DroitProfilServiceImpl();
        }
        return instance;
    }

    @Override
    public String listActionsByProfilMenu(ProfilMenu profilMenu) throws Exception {
        String listAsString = "";
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("profil.profilCode", OperateurCond.EQUAL, profilMenu.getProfil().getProfilCode()));
        criteres.add(new Critere("screen.screenId", OperateurCond.EQUAL, profilMenu.getMenu().getScreenId().getScreenId()));
        List<DroitProfil> droits = findBy(criteres);

        if (droits != null && droits.size() >= 1) {
            listAsString += droits.get(0).getAction().getActionNom();
            for (DroitProfil droitProfil : droits.subList(1, droits.size())) {
                listAsString += " / " + droitProfil.getAction().getActionNom();
            }
        }

        return listAsString;
    }

    @Override
    public List<DroitProfil> listByProfilMenu(Profil profil, Screen screen) throws Exception {
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("profil.profilCode", OperateurCond.EQUAL, profil.getProfilCode()));
        criteres.add(new Critere("screen.screenId", OperateurCond.EQUAL, screen.getScreenId()));
        return findBy(criteres);
    }
}