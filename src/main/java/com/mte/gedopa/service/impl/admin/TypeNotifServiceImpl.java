
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.TypeNotif;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.TypeNotifService;


/**
 *
 * @author H
 */
public class TypeNotifServiceImpl extends ServiceImpl<TypeNotif, String> implements TypeNotifService{
    
    private static TypeNotifServiceImpl instance;
    
    private TypeNotifServiceImpl(){
        
    }
    
    public static TypeNotifServiceImpl getInstance(){
        if(instance == null){
            instance = new TypeNotifServiceImpl();
        }
        return instance;
    }
}