
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Hypothesediag;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.HypothesediagService;


/**
 *
 * @author H
 */
public class HypothesediagServiceImpl extends ServiceImpl<Hypothesediag, Integer> implements HypothesediagService{
    
    private static HypothesediagServiceImpl instance;
    
    private HypothesediagServiceImpl(){
        
    }
    
    public static HypothesediagServiceImpl getInstance(){
        if(instance == null){
            instance = new HypothesediagServiceImpl();
        }
        return instance;
    }
}