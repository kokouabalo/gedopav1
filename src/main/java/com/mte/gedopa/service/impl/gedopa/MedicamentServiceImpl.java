
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Medicament;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.MedicamentService;


/**
 *
 * @author H
 */
public class MedicamentServiceImpl extends ServiceImpl<Medicament, String> implements MedicamentService{
    
    private static MedicamentServiceImpl instance;
    
    private MedicamentServiceImpl(){
        
    }
    
    public static MedicamentServiceImpl getInstance(){
        if(instance == null){
            instance = new MedicamentServiceImpl();
        }
        return instance;
    }
}