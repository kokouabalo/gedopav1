
package com.mte.gedopa.service.impl.gedopa;

import com.mte.gedopa.entities.Specialite;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.gedopa.SpecialiteService;


/**
 *
 * @author H
 */
public class SpecialiteServiceImpl extends ServiceImpl<Specialite, Integer> implements SpecialiteService{
    
    private static SpecialiteServiceImpl instance;
    
    private SpecialiteServiceImpl(){
        
    }
    
    public static SpecialiteServiceImpl getInstance(){
        if(instance == null){
            instance = new SpecialiteServiceImpl();
        }
        return instance;
    }
}