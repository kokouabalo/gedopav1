
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.Screen;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ScreenService;


/**
 *
 * @author H
 */
public class ScreenServiceImpl extends ServiceImpl<Screen, Integer> implements ScreenService{
    
    private static ScreenServiceImpl instance;
    
    private ScreenServiceImpl(){
        
    }
    
    public static ScreenServiceImpl getInstance(){
        if(instance == null){
            instance = new ScreenServiceImpl();
        }
        return instance;
    }
}