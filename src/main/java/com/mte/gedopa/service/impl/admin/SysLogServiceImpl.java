
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.SysLog;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.SysLogService;
import java.math.BigDecimal;


/**
 *
 * @author H
 */
public class SysLogServiceImpl extends ServiceImpl<SysLog, BigDecimal> implements SysLogService{
    
    private static SysLogServiceImpl instance;
    
    private SysLogServiceImpl(){
        
    }
    
    public static SysLogServiceImpl getInstance(){
        if(instance == null){
            instance = new SysLogServiceImpl();
        }
        return instance;
    }
}