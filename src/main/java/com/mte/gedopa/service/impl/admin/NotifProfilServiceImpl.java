
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.NotifProfil;
import com.mte.gedopa.entities.NotifProfilPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.NotifProfilService;


/**
 *
 * @author H
 */
public class NotifProfilServiceImpl extends ServiceImpl<NotifProfil, NotifProfilPK> implements NotifProfilService{
    
    private static NotifProfilServiceImpl instance;
    
    private NotifProfilServiceImpl(){
        
    }
    
    public static NotifProfilServiceImpl getInstance(){
        if(instance == null){
            instance = new NotifProfilServiceImpl();
        }
        return instance;
    }
}