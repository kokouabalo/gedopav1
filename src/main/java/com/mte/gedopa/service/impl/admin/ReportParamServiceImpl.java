
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.ReportParam;
import com.mte.gedopa.entities.ReportParamPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.ReportParamService;


/**
 *
 * @author H
 */
public class ReportParamServiceImpl extends ServiceImpl<ReportParam, ReportParamPK> implements ReportParamService{
    
    private static ReportParamServiceImpl instance;
    
    private ReportParamServiceImpl(){
        
    }
    
    public static ReportParamServiceImpl getInstance(){
        if(instance == null){
            instance = new ReportParamServiceImpl();
        }
        return instance;
    }
}