
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.entities.UserReport;
import com.mte.gedopa.entities.UserReport;
import com.mte.gedopa.entities.UserReportPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.UserReportService;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author H
 */
public class UserReportServiceImpl extends ServiceImpl<UserReport, UserReportPK> implements UserReportService{
    
    private static UserReportServiceImpl instance;
    
    private UserReportServiceImpl(){
        
    }
    
    public static UserReportServiceImpl getInstance(){
        if(instance == null){
            instance = new UserReportServiceImpl();
        }
        return instance;
    }

    @Override
    public List<Report> findByUser(AppUser appUser, boolean actifOnly) {
        List<Report> list = new ArrayList<>();
        if(appUser == null){
            return list;
        }
        List<Critere> criteres = new ArrayList<>();
        criteres.add(new Critere("appUser.userLogin", OperateurCond.EQUAL, appUser.getUserLogin()));
        if(actifOnly){
            criteres.add(new Critere("actif", OperateurCond.EQUAL, true));
        }
        try {
            List<UserReport> userReports = UserReportServiceImpl.getInstance().findBy(criteres);
            if(userReports != null && !userReports.isEmpty()){
                for (UserReport userReport : userReports) {
                    list.add(userReport.getReport());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(UserReportServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}