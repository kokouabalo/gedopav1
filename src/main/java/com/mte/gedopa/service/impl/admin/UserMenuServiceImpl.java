
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.entities.UserMenu;
import com.mte.gedopa.entities.UserMenuPK;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.UserMenuService;


/**
 *
 * @author H
 */
public class UserMenuServiceImpl extends ServiceImpl<UserMenu, UserMenuPK> implements UserMenuService{
    
    private static UserMenuServiceImpl instance;
    
    private UserMenuServiceImpl(){
        
    }
    
    public static UserMenuServiceImpl getInstance(){
        if(instance == null){
            instance = new UserMenuServiceImpl();
        }
        return instance;
    }
}