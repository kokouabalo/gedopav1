
package com.mte.gedopa.service.impl.admin;

import com.mte.gedopa.dao.DaoImpl;
import com.mte.gedopa.dao.impl.AppSeqDao;
import com.mte.gedopa.entities.AppSeq;
import com.mte.gedopa.service.ServiceImpl;
import com.mte.gedopa.service.admin.AppSeqService;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author H
 */
public class AppSeqServiceImpl extends ServiceImpl<AppSeq, String> implements AppSeqService{
    
    private static AppSeqServiceImpl instance;
    
    private AppSeqServiceImpl(){
        
    }
    
    public static AppSeqServiceImpl getInstance(){
        if(instance == null){
            instance = new AppSeqServiceImpl();
        }
        return instance;
    }

    @Override
    public DaoImpl<AppSeq, String> getDao() {
        return new AppSeqDao();
    }
    
    

    @Override
    public synchronized AppSeq getCurrentVal(String seqName) throws Exception {
        return  ((AppSeqDao)getDao()).getCurrentVal(seqName);
    }

    @Override
    public synchronized AppSeq getNextVal(String seqName) throws Exception {
        return  getNextVal(seqName, true);
    }

    @Override
    public synchronized AppSeq getNextVal(String seqName, boolean autoCommit) throws Exception {
        return  ((AppSeqDao)getDao()).getNextVal(seqName, autoCommit);
    }

    @Override
    public String formatVal(AppSeq appSeq) throws Exception {
        String formatedVal = format(String.valueOf(appSeq.getSeqVal()), appSeq.getSeqLength().intValue(), '0');
        return formatedVal;
    }

    @Override
    public String getValeurComplete(AppSeq appSeq) throws Exception {
        String formatedVal = formatVal(appSeq);
        String prefixe = appSeq.getSeqPref() != null ? appSeq.getSeqPref() : "";
        if(appSeq.getPatternPref()){
            prefixe = (new SimpleDateFormat(prefixe)).format(new Date());
        }
        
        String suffixe = appSeq.getSeqSuf()!= null ? appSeq.getSeqSuf() : "";
        if(appSeq.getPatternSuf()){
            suffixe = (new SimpleDateFormat(suffixe)).format(new Date());
        }
        
        return prefixe + formatedVal + suffixe;
    }
    
    private String format(String racine, int longueur, char c){
        String valeur = racine;
        if(longueur > racine.length()){
            for (int i = 0; i < longueur - racine.length(); i++) {
                valeur = String.valueOf(c) + valeur;
            }
        }
        return valeur;
    }
}