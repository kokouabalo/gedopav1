
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Position;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface PositionService extends Service<Position, String>{
    
}