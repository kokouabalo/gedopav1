
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.UserMenu;
import com.mte.gedopa.entities.UserMenuPK;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface UserMenuService extends Service<UserMenu, UserMenuPK>{
    
}