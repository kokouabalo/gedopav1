
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Param;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ParamService extends Service<Param, Integer>{
    
}