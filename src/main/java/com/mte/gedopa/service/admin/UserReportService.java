
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.entities.UserReport;
import com.mte.gedopa.entities.UserReportPK;
import com.mte.gedopa.service.Service;
import java.util.List;


/**
 *
 * @author H
 */
public interface UserReportService extends Service<UserReport, UserReportPK>{
    
    /**
     * Retourne la liste des reports attribués au profil et ceux attribués directement à l'utilisateur en paramètre
     * @param appUser l'utilisateur
     * @param actifOnly
     * @return la liste des reports attribués ou une liste vide si aucune attribution trouvée
     */
    public List<Report> findByUser(AppUser appUser, boolean actifOnly);
    
}