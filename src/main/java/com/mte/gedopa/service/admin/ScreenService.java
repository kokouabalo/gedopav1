
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Screen;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ScreenService extends Service<Screen, Integer>{
    
}