
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.ReportParam;
import com.mte.gedopa.entities.ReportParamPK;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ReportParamService extends Service<ReportParam, ReportParamPK>{
    
}