
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.ProfilMenu;
import com.mte.gedopa.entities.ProfilMenuPK;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ProfilMenuService extends Service<ProfilMenu, ProfilMenuPK>{
    
}