
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.NotifProfil;
import com.mte.gedopa.entities.NotifProfilPK;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface NotifProfilService extends Service<NotifProfil, NotifProfilPK>{
    
}