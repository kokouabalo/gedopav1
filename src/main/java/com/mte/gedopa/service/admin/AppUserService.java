
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.Menu;
import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.service.Service;
import java.util.List;


/**
 *
 * @author H
 */
public interface AppUserService extends Service<AppUser, String>{
    /**
     * Verifie si le login et le mot de passe saisis existent une seule fois ds
     * la BD, et retourne alors le code du profile de l'utilisateur
     * correspondant, ou -1 sinon
     *
     * @param login le login saisi
     * @param password le password saisi
     * @param operDesc Texte descriptive de l'opération (Nom/Adresse IP de la machine client, etc.)
     * @return l'utilisateur trouvé
     * @throws java.lang.Exception
     */
    public AppUser authentifier(String login, String password, String operDesc) throws Exception;
    
    /**
     * Déconnecte avec trace l'utilisateur spécifié en paramètre
     * @param appUser l'utilisateur à déconnecter
     * @param operDesc Texte descriptive de l'opération (Nom/Adresse IP de la machine client, etc.)
     */
    public void logout(AppUser appUser, String operDesc) throws Exception;
    
    /**
     * Retourne la liste des menus attribués au profil utilisateur en paramètre
     * @param profil
     * @param actifOnly
     * @return la liste des menus attribués ou une liste vide si aucune attribution trouvée
     */
    public List<Menu> findMenusByProfil(Profil profil, boolean actifOnly);
    
    /**
     * Retourne la liste des menus attribués directement à l'utilisateur en paramètre
     * @param appUser l'utilisateur
     * @param actifOnly
     * @return la liste des menus attribués ou une liste vide si aucune attribution trouvée
     */
    public List<Menu> findMenusByUser(AppUser appUser, boolean actifOnly);
    
    /**
     * Retourne la liste des menus attribués au profil et ceux attribués directement à l'utilisateur en paramètre
     * @param appUser l'utilisateur
     * @param actifOnly
     * @return la liste des menus attribués ou une liste vide si aucune attribution trouvée
     */
    public List<Menu> findMenus(AppUser appUser, boolean actifOnly);
    
    /**
     * Retourne la liste des états attribués au profil et ceux attribués directement à l'utilisateur en paramètre
     * @param appUser l'utilisateur
     * @param actifOnly
     * @return la liste des états attribués ou une liste vide si aucune attribution trouvée
     */
    public List<Report> findReports(AppUser appUser, boolean actifOnly);
    
}