
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.DroitUser;
import com.mte.gedopa.entities.DroitUserPK;
import com.mte.gedopa.entities.Screen;
import com.mte.gedopa.entities.UserMenu;
import com.mte.gedopa.service.Service;
import java.util.List;


/**
 *
 * @author H
 */
public interface DroitUserService extends Service<DroitUser, DroitUserPK>{
    public String listActionsByUserMenu(UserMenu userMenu) throws Exception;
    public List<DroitUser> listByUserAndScreen(AppUser appUser, Screen screen) throws Exception;
    
}