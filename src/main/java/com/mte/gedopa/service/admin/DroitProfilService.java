
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.DroitProfil;
import com.mte.gedopa.entities.DroitProfilPK;
import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.entities.ProfilMenu;
import com.mte.gedopa.entities.Screen;
import com.mte.gedopa.service.Service;
import java.util.List;


/**
 *
 * @author H
 */
public interface DroitProfilService extends Service<DroitProfil, DroitProfilPK>{
    public String listActionsByProfilMenu(ProfilMenu profilMenu) throws Exception;
    public List<DroitProfil> listByProfilMenu(Profil profil, Screen screen) throws Exception;
    
}