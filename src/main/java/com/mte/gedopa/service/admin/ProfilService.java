
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ProfilService extends Service<Profil, String>{
    
}