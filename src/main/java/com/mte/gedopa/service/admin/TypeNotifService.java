
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.TypeNotif;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface TypeNotifService extends Service<TypeNotif, String>{
    
}