
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.AppSeq;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface AppSeqService extends Service<AppSeq, String>{
    
    /**
     * Retourne un objet AppSeq correspondant au nom seqname et dont la valeur est la valeur courante
     * @param seqname
     * @return La valeur courante de la séquence
     * @throws Exception
     */
    public AppSeq getCurrentVal(String seqname) throws Exception;

    /**
     * Retourne un objet AppSeq correspondant au nom seqname et dont la valeur est la valeur courante+1, 
     * puis met à jour la séquence dans la BD.
     * @param seqname
     * @return La valeur courante incrémentée de 1 de la séquence
     * @throws Exception
     */
    public AppSeq getNextVal(String seqname) throws Exception;
    
    public AppSeq getNextVal(String seqname, boolean autoCommit) throws Exception;

    /**
     * Retourne la valeur de la séquence appSeq sur une certaine longueur spécifiée par seq_length
     * @param appSeq la séquence dont on veut une valeur formatée
     * @return La valeur formatté de la séquence au format n zéros+NEXT_VAL. Exple: 000123, si nextVal = 123 et seq_length=6
     * @throws Exception
     */
    public String formatVal(AppSeq appSeq) throws Exception;

    /**
     * Retourne la valeur complète de la séquence appSeq
     * @param appSeq la séquence dont on veut une valeur complète
     * @return La valeur complète de la séquence au format PREFIXE+NUMERO_FORMATE+SUFFIXE. Exple: FC000123/PAL/DC
     * @throws Exception
     */
    public String getValeurComplete(AppSeq appSeq) throws Exception;
    
}