
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Action;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ActionService extends Service<Action, String>{
    
}