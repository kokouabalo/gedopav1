
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.App;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface AppService extends Service<App, Integer>{
    
}