
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.ReportType;
import com.mte.gedopa.service.Service;


/**
 *
 * @author H
 */
public interface ReportTypeService extends Service<ReportType, String>{
    
}