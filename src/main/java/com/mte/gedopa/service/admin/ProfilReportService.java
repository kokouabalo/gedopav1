
package com.mte.gedopa.service.admin;
import com.mte.gedopa.entities.Profil;
import com.mte.gedopa.entities.ProfilReport;
import com.mte.gedopa.entities.ProfilReportPK;
import com.mte.gedopa.entities.Report;
import com.mte.gedopa.service.Service;
import java.util.List;


/**
 *
 * @author H
 */
public interface ProfilReportService extends Service<ProfilReport, ProfilReportPK>{
    
    /**
     * Retourne la liste des reports attribués au profil utilisateur en paramètre
     * @param profil
     * @param actifOnly
     * @return la liste des reports attribués ou une liste vide si aucune attribution trouvée
     */
    public List<Report> findByProfil(Profil profil, boolean actifOnly);
    
}