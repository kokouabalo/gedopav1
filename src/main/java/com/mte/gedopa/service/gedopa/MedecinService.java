
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Medecin;
import com.mte.gedopa.service.Service;


/**
 *
 * @author HP
 */
public interface MedecinService extends Service<Medecin, String>{
    
}