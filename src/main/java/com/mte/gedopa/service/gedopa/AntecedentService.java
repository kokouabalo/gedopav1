
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Antecedent;
import com.mte.gedopa.service.Service;


/**
 *
 * @author HP
 */
public interface AntecedentService extends Service<Antecedent, Integer>{
    
}