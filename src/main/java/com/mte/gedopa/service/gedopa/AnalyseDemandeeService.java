
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.AnalyseDemandee;
import com.mte.gedopa.entities.AnalyseDemandeePK;
import com.mte.gedopa.service.Service;

/**
 *
 * @author HP
 */

public interface AnalyseDemandeeService extends Service<AnalyseDemandee, AnalyseDemandeePK>{
    
}