
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Consultation;
import com.mte.gedopa.service.Service;


/**
 *
 * @author HP
 */
public interface ConsultationService extends Service<Consultation, Long>{
    
}