
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.Medicament;
import com.mte.gedopa.service.Service;


/**
 *
 * @author HP
 */
public interface MedicamentService extends Service<Medicament, String>{
    
}