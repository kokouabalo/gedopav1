
package com.mte.gedopa.service.gedopa;
import com.mte.gedopa.entities.HypothesediagCons;
import com.mte.gedopa.entities.HypothesediagConsPK;
import com.mte.gedopa.service.Service;

/**
 *
 * @author HP
 */

public interface HypothesediagConsService extends Service<HypothesediagCons, HypothesediagConsPK>{
    
}