/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.service;


import com.mte.gedopa.dao.DaoImpl;
import com.mte.gedopa.entities.Action;
import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.SysLog;
import com.mte.gedopa.service.impl.admin.ActionServiceImpl;
import com.mte.gedopa.service.impl.admin.SysLogServiceImpl;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.ICritere;
import com.mte.gedopa.util.Logs;
import com.mte.gedopa.util.MinMax;
import com.mte.gedopa.util.OperateurSql;
import com.mte.gedopa.util.SortConfig;
import java.lang.reflect.ParameterizedType;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


/**
 *
 * @author SIC
 * @param <T>
 * @param <K>
 */
public class ServiceImpl<T, K> implements Service<T, K> {
    
    private final String packageName = "com.mte.gedopa.dao.impl",daoEnd = "Dao";
    
    @Override
    public List<T> read() throws Exception {
        return read(null);
    }
    
    @Override
    public List<T> read(List<SortConfig> sortConfigs) throws Exception {
        return getDao().read(sortConfigs);
    }

    @Override
    public T create(T entity, Logs noLog) throws Exception {
        return create(entity, true, noLog);
    }

    @Override
    public T create(T entity, AppUser appUser, String operDesc) throws Exception {
        return create(entity, true, appUser, operDesc);
    }

    @Override
    public T update(T entity, AppUser appUser, String operDesc) throws Exception {
        return update(entity, true, appUser, operDesc);
    }

    @Override
    public T merge(T entity, AppUser appUser, String operDesc) throws Exception {
        return merge(entity, true, appUser, operDesc);
    }

    @Override
    public void delete(T entity, AppUser appUser, String operDesc) throws Exception {
        delete(entity, true, appUser, operDesc);
    }

    @Override
    public T findByPk(K pk) throws Exception {
        return getDao().findByPk(pk);
    }

    @Override
    public List<T> findBy(Map<String, Object> params, OperateurSql op) throws Exception {
        return getDao().findBy(params, op);
    }

    @Override
    public List<T> findBy(List<Critere> criteres, List<SortConfig> sortConfigs) throws Exception {
        return getDao().findBy(criteres, sortConfigs);
    }

    @Override
    public List<T> findByIcriteres(List<ICritere> criteres, List<SortConfig> sortConfigs) throws Exception {
        return getDao().findByIcriteres(criteres, sortConfigs);
    }

    @Override
    public DaoImpl<T, K> getDao() {
        Class<T> c = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        try {
            return (DaoImpl<T, K>) Class.forName(packageName + "." +c.getSimpleName() + daoEnd).newInstance();
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        } 
        return null;
    }

    @Override
    public T create(T entity, boolean autocommit, Logs noLog) throws Exception {
        return this.getDao().create(entity, autocommit);
    }

    @Override
    public T create(T entity, boolean autocommit, AppUser appUser, String operDesc) throws Exception {
        entity = this.getDao().create(entity, autocommit);
        log(getRealEntityName(), appUser, ActionServiceImpl.getInstance().findByPk("CREATE"), entity.toString() + (operDesc != null ? " :: " + operDesc : ""), autocommit);
        return entity;
    }

    @Override
    public T update(T entity, boolean autocommit, AppUser appUser, String operDesc) throws Exception {
        log(getRealEntityName(), appUser, ActionServiceImpl.getInstance().findByPk("UPDATE"), (operDesc != null ? " :: " + operDesc : ""), autocommit);
        return this.getDao().update(entity, autocommit);
    }

    @Override
    public T merge(T entity, boolean autocommit, AppUser appUser, String operDesc) throws Exception {
        log(getRealEntityName(), appUser, ActionServiceImpl.getInstance().findByPk("UPDATE"), (operDesc != null ? " :: " + operDesc : ""), autocommit);
        return this.getDao().merge(entity, autocommit);
    }

    @Override
    public void delete(T entity, boolean autocommit, AppUser appUser, String operDesc) throws Exception {
        log(getRealEntityName(), appUser, ActionServiceImpl.getInstance().findByPk("DELETE"), (operDesc != null ? " :: " + operDesc : ""), autocommit);
        this.getDao().delete(entity, autocommit);
    }

    @Override
    public K findMinMaxIdBy(List<Critere> criteres, MinMax extremum, String idProperty) throws Exception {
        return this.getDao().findMinMaxIdBy(criteres, extremum, idProperty);
    }

    @Override
    public K findMinMaxIdByIcriteres(List<ICritere> criteres, MinMax extremum, String idProperty) throws Exception {
        return this.getDao().findMinMaxIdByIcriteres(criteres, extremum, idProperty);
    }

    @Override
    public List<T> findBy(List<Critere> criteres) throws Exception {
        return findBy(criteres, null);
    }

    @Override
    public List<T> findByIcriteres(List<ICritere> criteres) throws Exception {
        return findByIcriteres(criteres, null);
    }
    
    protected void log(String entityName, AppUser appUser, Action action, String operDesc, boolean autocommit) throws Exception{
        SysLog log = new SysLog();
        log.setUserLogin(appUser);
        log.setActionCode(action);
        log.setLogDate(Calendar.getInstance().getTime());
        log.setLogDesc(operDesc);
        log.setEntity(entityName);
        log.setEntityId(null);
        SysLogServiceImpl.getInstance().create(log, autocommit, Logs.NO_LOG);
    }
    
    private String getRealEntityName(){
        Class<T> c = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return c.getSimpleName();
    }
    
}
