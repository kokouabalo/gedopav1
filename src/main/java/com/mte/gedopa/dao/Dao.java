/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.dao;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.ICritere;
import com.mte.gedopa.util.MinMax;
import com.mte.gedopa.util.OperateurSql;
import com.mte.gedopa.util.SortConfig;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author H
 * @param <T>
 * @param <K>
 */
public interface Dao<T,K> extends Serializable{
    
    /**
     * Recherche toutes les occurences dans la base
     * @return result list
     * @throws java.lang.Exception
     */
    public List<T> read() throws Exception;
    
    /**
     * Recherche toutes les occurences dans la base
     * @param sortConfigs La configuration des champs de tri
     * @return result list
     * @throws java.lang.Exception
     */
    public List<T> read(List<SortConfig> sortConfigs) throws Exception;
    
    /**
     * Créer une nouvelle occurrence dans la base
     * @param entity to create
     * @return the entity after its persistence
     * @throws java.lang.Exception
     */
    public T create(T entity) throws Exception;
    
    /**
     * Met à jour une occurrence dans la base
     * @param entity to update
     * @return la nouvelle entité mise à jour
     * @throws java.lang.Exception
     */
    public T update(T entity) throws Exception;
    
    /**
     * Met à jour une occurrence dans la base
     * @param entity to update
     * @return la nouvelle entité mise à jour
     * @throws java.lang.Exception
     */
    public T merge(T entity) throws Exception;
    
    /**
     * Supprime une occurrence dans la base
     * @param t l'entité à supprimer
     * @throws java.lang.Exception
     */
    public void delete(T t) throws Exception;
    
    /**
     * Retrouve une entité par son identifiant
     * @param pk of the entity to find
     * @return the result entity
     * @throws java.lang.Exception
     */
    public T findByPk(K pk) throws Exception;
    
    /**
     * Trouve une liste d'entités à partir de certaines colonnes valuées et d'un opérateur
     * @param params : the list of columns which participate to the search
     * @param op : the operator to use in the search
     * @return the result list
     * @throws java.lang.Exception
     */
    public List<T> findBy(Map<String,Object> params,OperateurSql op) throws Exception;
    
    /**
     * Trouve une liste d'entités à partir d'une liste de critères
     * @param criteres La liste de critères de recherche
     * @return the result list
     * @throws java.lang.Exception
     */
    public List<T> findBy(List<Critere> criteres) throws Exception;
    
    /**
     * Trouve une liste d'entités à partir d'une liste de critères groupés
     * @param criteres La liste de critères de recherche
     * @return the result list
     * @throws java.lang.Exception
     */
    public List<T> findByIcriteres(List<ICritere> criteres) throws Exception;
    
    /**
     * Trouve une liste d'entités à partir d'une liste de critères
     * @param criteres La liste de critères de recherche
     * @param sortConfigs La configuration des champs de tri
     * @return the result list
     * @throws java.lang.Exception
     */
    public List<T> findBy(List<Critere> criteres, List<SortConfig> sortConfigs) throws Exception;
    
    /**
     * Trouve une liste d'entités à partir d'une liste de critères groupés
     * @param criteres La liste de critères de recherche
     * @param sortConfigs La configuration des champs de tri
     * @return the result list
     * @throws java.lang.Exception
     */
    public List<T> findByIcriteres(List<ICritere> criteres, List<SortConfig> sortConfigs) throws Exception;
    
    
    
    /**
     * Trouve l'ID du dernier enregistrement saisie à partir d'une liste de critères
     * @param criteres La liste de critères de recherche
     * @param extremum la fonction extremum (MIN ou MAX)
     * @param idProperty la propriété Id de l'entité
     * @return l'ID du dernier enregistrement
     * @throws java.lang.Exception
     */
    public K findMinMaxIdBy(List<Critere> criteres, MinMax extremum, String idProperty) throws Exception;
    
    /**
     * Trouve l'ID du dernier enregistrement saisie à partir d'une liste de critères
     * @param criteres La liste de Icritères de recherche
     * @param extremum la fonction extremum (MIN ou MAX)
     * @param idProperty la propriété Id de l'entité
     * @return l'ID du dernier enregistrement
     * @throws java.lang.Exception
     */
    public K findMinMaxIdByIcriteres(List<ICritere> criteres, MinMax extremum, String idProperty) throws Exception;
    
    /**
     * Trouve la classe de l'entité concernée par le Dao
     * @return the result class 
     */
    public Class<T> getEntityClass();
    
    /**
     * Crée une copie de l'entité en paramètre
     * @param original : the entity to clone
     * @return the result copy
     * @throws java.lang.Exception
     */
    public T clone(T original) throws Exception ;
    
    /**
     * Créer une nouvelle occurrence dans la base
     * @param entity
     * @param autocommit
     * @return 
     * @throws java.lang.Exception
     */
    public T create(T entity, boolean autocommit) throws Exception;
    
    /**
     * Met à jour une occurrence dans la base
     * @param entity
     * @param autocommit
     * @return la nouvelle entité mise à jour
     * @throws java.lang.Exception
     */
    public T update(T entity, boolean autocommit) throws Exception;
    
    /**
     * Met à jour une occurrence dans la base
     * @param entity
     * @param autocommit
     * @return la nouvelle entité mise à jour
     * @throws java.lang.Exception
     */
    public T merge(T entity, boolean autocommit) throws Exception;
    
    /**
     * Supprime une occurrence dans la base
     * @param entity
     * @param autocommit
     * @throws java.lang.Exception
     */
    public void delete(T entity, boolean autocommit) throws Exception;
    
    
}
