
package com.mte.gedopa.dao.impl;
import com.mte.gedopa.entities.ProfilMenu;
import com.mte.gedopa.dao.DaoImpl;
import com.mte.gedopa.entities.ProfilMenuPK;


/**
 *
 * @author Horacio Tonou (SIC)
 */
public class ProfilMenuDao extends DaoImpl<ProfilMenu, ProfilMenuPK> {
    
}