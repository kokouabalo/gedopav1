
package com.mte.gedopa.dao.impl;
import com.mte.gedopa.dao.DaoImpl;
import com.mte.gedopa.entities.Prescription;
import com.mte.gedopa.entities.PrescriptionPK;


/**
 *
 * @author H
 */
public class PrescriptionDao extends DaoImpl<Prescription, PrescriptionPK> {
    
}