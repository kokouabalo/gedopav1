
package com.mte.gedopa.dao.impl;
import com.mte.gedopa.dao.DaoImpl;
import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.util.CritereSimple;
import com.mte.gedopa.util.OperateurCond;


/**
 *
 *@author H
 */
public class PatientDao extends DaoImpl<Patient, Long> {   
    private String getQueryToken(CritereSimple critere, int compteur, boolean addOpSql) {
        String resultQuery = "";
        resultQuery += (addOpSql ? critere.getOpSql().toString() : "");

        String condition;
        if (critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString())) {
            if (critere.getOpCond().equals(OperateurCond.EQUAL)) {
                condition = " is null ";
            } else {
                condition = " is not null ";
            }

        } else {// valeur non nulle
            condition = " " + critere.getOpCond() + " :p" + compteur + " ";

        }
        resultQuery += " d.idAnnonceEscale." + critere.getPropriete() + condition;
        return resultQuery;
    }
}