package com.mte.gedopa.dao.impl;

import com.mte.gedopa.dao.DaoImpl;
import com.mte.gedopa.entities.AppSeq;
import com.mte.gedopa.util.jdbc.Connector;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author H
 */
public class AppSeqDao extends DaoImpl<AppSeq, String> {

    public AppSeq getCurrentVal(String seqName) throws Exception {
        return findByPk(seqName);
    }

    public AppSeq getNextVal(String seqName) throws Exception {

        return getNextVal(seqName, true);
    }

    public synchronized AppSeq getNextVal(String seqName, boolean autoCommit) throws Exception {
        AppSeq next = findByPkWithJdbc(seqName);
        next.setSeqVal(next.getSeqVal() + 1L);
        updateByJdbc(next, autoCommit);

        return next;
    }

    private AppSeq findByPkWithJdbc(String seqName) throws Exception {
        AppSeq appSeq = null;
        Connection conn = Connector.getAdminConnection();
        String sql = "SELECT SEQ_NAME, SEQ_VAL, SEQ_PREF, SEQ_SUF, SEQ_LENGTH, PATTERN_PREF, PATTERN_SUF "
                + " FROM PUBLIC.APP_SEQ "
                + " WHERE SEQ_NAME = ? ";
        PreparedStatement ps2 = conn.prepareStatement(sql);
        ps2.setString(1, seqName);
        ResultSet rs = ps2.executeQuery();
        if (rs.next()) {
            appSeq = new AppSeq();
            appSeq.setSeqName(rs.getString(1));
            appSeq.setSeqVal(rs.getLong(2));
            appSeq.setSeqPref(rs.getString(3));
            appSeq.setSeqSuf(rs.getString(4));
            appSeq.setSeqLength(new BigInteger(rs.getString(5)));
            appSeq.setPatternPref(rs.getBoolean(6));
            appSeq.setPatternSuf(rs.getBoolean(7));
        }
        rs.close();
        ps2.close();
        conn.close();
        return appSeq;
    }

    private void updateByJdbc(AppSeq appSeq, boolean autoCommit) throws Exception {
        Connection conn = Connector.getAdminConnection();
        String sql = "UPDATE PUBLIC.APP_SEQ "
                + " SET SEQ_VAL = ? "
                + " WHERE SEQ_NAME = ? ";
        PreparedStatement ps2 = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps2.setLong(1, appSeq.getSeqVal());
        ps2.setString(2, appSeq.getSeqName());
        ps2.setQueryTimeout(5);
        conn.setAutoCommit(false);
        int count = ps2.executeUpdate();
        if (count == 1) {
            if(autoCommit){
                conn.commit();
            }
        }else{
            if(autoCommit){
                conn.rollback();
            }
        }
        ps2.close();
        conn.close();
    }

}
