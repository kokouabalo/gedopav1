
package com.mte.gedopa.dao.impl;
import com.mte.gedopa.entities.SysLog;
import com.mte.gedopa.dao.DaoImpl;
import java.math.BigDecimal;


/**
 *
 * @author Horacio Tonou (SIC)
 */
public class SysLogDao extends DaoImpl<SysLog, BigDecimal> {
    
}