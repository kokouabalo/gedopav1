
package com.mte.gedopa.dao.impl;
import com.mte.gedopa.dao.DaoImpl;
import com.mte.gedopa.entities.Consultation;


/**
 *
 *@author H
 */
public class ConsultationDao extends DaoImpl<Consultation, Long> {
     
   /* public List<AnnonceEscale> abriSecuritaireNavireEnRade(String codeTypeEscale, Date referenceDate) throws Exception{
    
        String query = "SELECT a FROM AnnonceEscale a where a.dateArriveeRade is not null AND a.dateSortieRade is null and a.idTypeEscale.codeTypeEscale=:p0 and ((a.dateDerniereDmdFact is not null and a.dateDerniereDmdFact< :p1)OR (a.dateDerniereDmdFact is  null AND a.dateArriveeRade< :p1))  ";

        System.out.println(query);

        Query q = getEm().createQuery(query);
        q.setParameter("p0", codeTypeEscale);
        q.setParameter("p1", referenceDate);
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        
        return q.getResultList();
    }
    
   
    public List<AnnonceEscale> abriSecuritaireNavireParti(String codeTypeEscale) throws Exception {
        String query = "SELECT a FROM AnnonceEscale a where a.dateArriveeRade is not null AND a.dateSortieRade is not null and a.idTypeEscale.codeTypeEscale=:p0 and (a.dateDerniereDmdFact is null OR a.dateDerniereDmdFact<a.dateSortieRade)  ";

        System.out.println(query);

        Query q = getEm().createQuery(query);
        q.setParameter("p0",codeTypeEscale);
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        return q.getResultList();
    }
    
    public List<AnnonceEscale> listEscaleAbriSecuritaire (String codeTypeEscale, Date referenceDate)throws Exception{
        List<AnnonceEscale> listTotal=new ArrayList<>();
        
        listTotal.clear();
        
        //REcherche des navires en Rade pour Facturation
        String query = "SELECT a FROM AnnonceEscale a where a.dateArriveeRade is not null AND a.dateSortieRade is null and a.idTypeEscale.codeTypeEscale=:p0 and ((a.dateDerniereDmdFact is not null and a.dateDerniereDmdFact< :p1)OR (a.dateDerniereDmdFact is  null ))  ";

        System.out.println(query);

        Query q = getEm().createQuery(query);
        q.setParameter("p0", codeTypeEscale);
        q.setParameter("p1", referenceDate);
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        
        listTotal.addAll(q.getResultList());
        
        //Recherche des navires partis non facturés
        String query1 = "SELECT a FROM AnnonceEscale a where a.dateArriveeRade is not null AND a.dateSortieRade is not null and a.idTypeEscale.codeTypeEscale=:p0 and (a.dateDerniereDmdFact is null OR a.dateDerniereDmdFact<a.dateSortieRade)  ";

        System.out.println(query1);

        Query q1 = getEm().createQuery(query1);
        q1.setParameter("p0",codeTypeEscale);
        q1.setHint(QueryHints.REFRESH, HintValues.TRUE);
        listTotal.addAll(q1.getResultList());
        
        return listTotal;
    }
   
    public List<AnnonceEscale> findByIdManutPort(Long idClient,List<String> situEscaleList) throws Exception{
        
        String conditionEscale="(";
        int compteur=0;
        for(String situCode:situEscaleList){
            if(compteur>0){
                conditionEscale=conditionEscale+",";
            }
            conditionEscale=conditionEscale+"'"+situCode+"'";
            compteur++;
        }
        conditionEscale=conditionEscale.concat(")");
       
        String query = "SELECT A FROM AnnonceEscale A  "
                + "LEFT JOIN  DmdFactManutPort D  ON A=D.idAnnonceEscale "
                + "WHERE D.idAnnonceEscale is NULL AND A IN (SELECT O.annonceEscale FROM OperationEscale O  WHERE O.manutentionnaire.idClient = :p) AND  A.codeSituEscale.codeSituEscale IN "+conditionEscale;
        System.out.println(query);
        
      
        Query q = getEm().createQuery(query);
        q.setParameter("p", idClient);
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
       
        return q.getResultList();
    }
    
    public List<AnnonceEscale> findByLigneStatut(List<ICritere> criteres, List<String> listeLigneStatut,String codeTypeMouvement) throws Exception {
        //List<DmdSrvNautique> listDmdSrvNautique = new ArrayList<>();
        String conditionLigneStatut = "(";
        String conditionCodeTypeMouvement="";
        int pas = 0;
        for (String LigneStatut : listeLigneStatut) {
            if (pas > 0) {
                conditionLigneStatut = conditionLigneStatut + ",";
            }
            conditionLigneStatut = conditionLigneStatut + "'" + LigneStatut + "'";
            pas++;
        }
        conditionLigneStatut = conditionLigneStatut.concat(")");
        
        if(codeTypeMouvement.length()>0){
            conditionCodeTypeMouvement=" AND a.codeTypeMouvement.codeTypeMouvement=:A ";
        }

        String query = "Select d.idAnnonceEscale from DmdSrvNautique d where d in ( select a.idDmdSrvNaut from DetDmdSrvNaut a WHERE a.statutLigne IN " + conditionLigneStatut +" "+ conditionCodeTypeMouvement+ ")  ";
        int compteur = 0;
         int groupIndex = 0;
         if (criteres != null && criteres.size() > 0) {
            query += " AND ";
        }
        for (ICritere iCritere : criteres) {
            // Vérification d'opérateur logique : AND,OR,WHERE
            if (iCritere instanceof CritereSimple) {
                CritereSimple critere = (CritereSimple) iCritere;
                if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                    compteur++;
                }
                query += getQueryToken(critere, compteur, (groupIndex++ != 0));
            } else if (iCritere instanceof CritereGroup) {
                CritereGroup group = (CritereGroup) iCritere;
                query += compteur == 0 ? " " : " " + group.getOpSql() + " ";
                query += "(";

                groupIndex = 0;//on réinitialise à zéro au début de chaque groupe
                for (CritereSimple critere : group.getCriteres()) {
                    if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                        compteur++;
                    }
                    query += getQueryToken(critere, compteur, (groupIndex++ != 0));
                }
                query += ") ";
            }

        }

        System.out.println(query);
        compteur = 0;
        Query q = getEm().createQuery(query);
        
        for (ICritere iCritere : criteres) {

            if (iCritere instanceof CritereSimple) {
                CritereSimple critere = (CritereSimple) iCritere;
                if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                    q.setParameter("p" + (++compteur), critere.getValeur());
                    System.out.println("p" + compteur + " = " + critere.getValeur());
                }
            } else if (iCritere instanceof CritereGroup) {
                for (CritereSimple critere : ((CritereGroup) iCritere).getCriteres()) {
                    if (!(critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString()))) {
                        q.setParameter("p" + (++compteur), critere.getValeur());
                        System.out.println("p" + compteur + " = " + critere.getValeur());
                    }
                }
            }

        }
        if(codeTypeMouvement.length()>0){
           q.setParameter("A" ,codeTypeMouvement );
        }
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        
        return q.getResultList();
    }
    
    private String getQueryToken(CritereSimple critere, int compteur, boolean addOpSql) {
        String resultQuery = "";
        resultQuery += (addOpSql ? critere.getOpSql().toString() : "");

        String condition;
        if (critere.getValeur() == null || "null".equalsIgnoreCase(critere.getValeur().toString())) {
            if (critere.getOpCond().equals(OperateurCond.EQUAL)) {
                condition = " is null ";
            } else {
                condition = " is not null ";
            }

        } else {// valeur non nulle
            condition = " " + critere.getOpCond() + " :p" + compteur + " ";

        }
        resultQuery += " d.idAnnonceEscale." + critere.getPropriete() + condition;
        return resultQuery;
    }
    
    public List<BigDecimal> listAnneeEscale() throws Exception{
        List<BigDecimal> anneeEscaleList=new ArrayList<>();
        String query = "SELECT DISTINCT EXTRACT(YEAR FROM A.dateAccostage) as anneeEscale FROM AnnonceEscale A WHERE anneeEscale is not null  ORDER BY anneeEscale DESC";

        System.out.println(query);

        Query q = getEm().createQuery(query);
       
        q.setHint(QueryHints.REFRESH, HintValues.TRUE);
        
        List<Object[]> results = q.getResultList();
        System.out.println("results.size()"+results.size());
        for (int i = 0; i < results.size(); i++) {
            Object arr = results.get(i);
            System.out.println("results.size()"+arr);
            anneeEscaleList.add((BigDecimal) arr);
        }
        return anneeEscaleList;
       
    }*/
}