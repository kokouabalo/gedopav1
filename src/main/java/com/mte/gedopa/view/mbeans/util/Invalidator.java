/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mte.gedopa.view.mbeans.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author DEGA
 */
public class Invalidator {
    
    public static void invalide(String summary, String detail) {
        FacesContext fc = FacesContext.getCurrentInstance();

        FacesMessage fm = new FacesMessage();
        fm.setSummary(summary);
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        fc.addMessage(null, fm);
        fc.renderResponse();
    }
    
}
