package com.mte.gedopa.view.mbeans.gedopa;

import com.mte.gedopa.util.OperateurSql;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import com.mte.gedopa.service.gedopa.MedicamentService;
import com.mte.gedopa.service.impl.gedopa.MedicamentServiceImpl;
import com.mte.gedopa.entities.Medicament;
import com.mte.gedopa.util.MessageProvider;
import com.mte.gedopa.view.mbeans.util.LoginBean;
import java.text.MessageFormat;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author H
 */
@ManagedBean(name = "medicamentBean")
@ViewScoped
public class MedicamentBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    
    private MedicamentService service;

    private int index;
    private Medicament formObject;
    private Medicament selectedObject;
    private List<Medicament> dataList = new ArrayList<>();
    private List<Medicament> filteredList;
    private String message;
    private boolean editMode;

    private String gridEmptyMessage = "";

    @PostConstruct
    public void init() {
        try {
            service = MedicamentServiceImpl.getInstance();
            gridEmptyMessage = getI18nMessage("msg.grid.load.empty");
            effacer();
        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(MedicamentBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enregistrer() {
        if (isEditMode()) {
            try {
                // Mode modif
                getService().update(getFormObject(), loginBean.getConnectedUser(), formObject.toString());
                dataList.set(index, formObject);
                setMessage(getI18nMessage("msg.action.update.success", formObject.getCodemedi()));
                showSuccesOppDialog();
                effacer();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.update.fail", formObject.getCodemedi(), ex.getMessage()));
                Logger.getLogger(MedicamentBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        } else {
            //Mode ajout
            try {
                Medicament entity = formObject;
                entity = getService().create(entity, loginBean.getConnectedUser(), entity.toString());
                getDataList().add(entity);
                setMessage(getI18nMessage("msg.action.create.success", entity.getCodemedi()));
                showSuccesOppDialog();
                effacer();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.create.fail", formObject.getCodemedi(), ex.getMessage()));
                Logger.getLogger(MedicamentBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    public void effacer() {
        setFormObject(new Medicament());
        setSelectedObject(null);
        setEditMode(false);
    }
    
    public void effacerTout(){
        effacer();
        dataList.clear();
    }

    public void rowSelect() {
        setFormObject(getSelectedObject());
        index = dataList.indexOf(selectedObject);
        setEditMode(true);
    }

    public void supprimer() {
        if (selectedObject == null) {
            setMessage(getI18nMessage("msg.action.delete.error.noselect"));
        } else {
            try {
                String id = selectedObject.getCodemedi();
                service.delete(selectedObject, loginBean.getConnectedUser(), selectedObject.toString());
                dataList.remove(index);
                effacer();
                setMessage(getI18nMessage("msg.action.delete.success", id));
                showSuccesOppDialog();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.delete.fail", selectedObject.getCodemedi(), ex.getMessage()));
                Logger.getLogger(MedicamentBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    //Confirmer la suppression
    public void confirmerSuppression() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confirmDelete').show();");
    }

    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOpp').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOpp').show();");
    }

    public void rechercher(boolean searchByAnd) {
        Map<String, Object> params = new HashMap<>();
        if (formObject.getCodemedi() != null && formObject.getCodemedi().length() > 0 && !"%".equals(formObject.getCodemedi())) {
            params.put("codemedi", formObject.getCodemedi());
        }
        if (formObject.getLibmedi() != null && formObject.getLibmedi().length() > 0 && !"%".equals(formObject.getLibmedi())) {
            params.put("libmedi", formObject.getLibmedi());
        }

        /*     if(formObject.getActif() != null) {
         params.put("actif", formObject.getActif());
         }
         */
        dataList.clear();
        try {
            dataList.addAll(service.findBy(params, (searchByAnd ? OperateurSql.AND : OperateurSql.OR)));
            if (dataList.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(MedicamentBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the service
     */
    public MedicamentService getService() {
        return service;
    }

    /**
     * @return the formObject
     */
    public Medicament getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
    public void setFormObject(Medicament formObject) {
        this.formObject = formObject;
    }

    /**
     * @return the selectedObject
     */
    public Medicament getSelectedObject() {
        return selectedObject;
    }

    /**
     * @param selectedObject the selectedObject to set
     */
    public void setSelectedObject(Medicament selectedObject) {
        this.selectedObject = selectedObject;
    }

    /**
     * @return the dataList
     */
    public List<Medicament> getDataList() {
        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(List<Medicament> dataList) {
        this.dataList = dataList;
    }

    /**
     * @return the filteredList
     */
    public List<Medicament> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Medicament> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }
  /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}
