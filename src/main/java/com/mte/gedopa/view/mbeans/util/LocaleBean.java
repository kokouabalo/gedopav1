/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.mbeans.util;
 
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author HP
 */
@ManagedBean(name = "localeBean")
@SessionScoped
public class LocaleBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String currentLang = "fr";
    private static final Map<String, Object> languages;

    static {
        languages = new LinkedHashMap<>();
        languages.put("fr", Locale.FRENCH); //label, value
        languages.put("en", Locale.ENGLISH);
        languages.put("x", Locale.PRIVATE_USE_EXTENSION);
        languages.put("zh_CN", Locale.SIMPLIFIED_CHINESE);
    }

    public void changeLocale(String localeCode) {
        this.currentLang = localeCode;
        FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) languages.get(localeCode));
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.setAttribute("currentLang", localeCode);
    }
    
    /**
     * @return the currentLang
     */
    public String getCurrentLang() {
        return currentLang;
    }

    /**
     * @param currentLang the currentLang to set
     */
    public void setCurrentLang(String currentLang) {
        this.currentLang = currentLang;
    }

}
