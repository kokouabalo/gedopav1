package com.mte.gedopa.view.mbeans.gedopa;

import com.mte.gedopa.util.OperateurSql;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import com.mte.gedopa.service.gedopa.SpecialiteService;
import com.mte.gedopa.service.impl.gedopa.SpecialiteServiceImpl;
import com.mte.gedopa.entities.Specialite;
import com.mte.gedopa.util.MessageProvider;
import com.mte.gedopa.view.mbeans.util.LoginBean;
import java.text.MessageFormat;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author H
 */
@ManagedBean(name = "specialiteBean")
@ViewScoped
public class SpecialiteBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    
    private SpecialiteService service;

    private int index;
    private Specialite formObject;
    private Specialite selectedObject;
    private List<Specialite> dataList = new ArrayList<>();
    private List<Specialite> filteredList;
    private String message;
    private boolean editMode;

    private String gridEmptyMessage = "";

    @PostConstruct
    public void init() {
        try {
            service = SpecialiteServiceImpl.getInstance();
            gridEmptyMessage = getI18nMessage("msg.grid.load.empty");
            effacer();
        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(SpecialiteBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enregistrer() {
        if (isEditMode()) {
            try {
                // Mode modif
                getService().update(getFormObject(), loginBean.getConnectedUser(), formObject.toString());
                dataList.set(index, formObject);
                setMessage(getI18nMessage("msg.action.update.success", formObject.getIdspec()));
                showSuccesOppDialog();
                effacer();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.update.fail", formObject.getIdspec(), ex.getMessage()));
                Logger.getLogger(SpecialiteBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        } else {
            //Mode ajout
            try {
                Specialite entity = formObject;
                entity = getService().create(entity, loginBean.getConnectedUser(), entity.toString());
                getDataList().add(entity);
                setMessage(getI18nMessage("msg.action.create.success", entity.getIdspec()));
                showSuccesOppDialog();
                effacer();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.create.fail", formObject.getIdspec(), ex.getMessage()));
                Logger.getLogger(SpecialiteBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    public void effacer() {
        setFormObject(new Specialite());
        setSelectedObject(null);
        setEditMode(false);
    }
    
    public void effacerTout(){
        effacer();
        dataList.clear();
    }

    public void rowSelect() {
        setFormObject(getSelectedObject());
        index = dataList.indexOf(selectedObject);
        setEditMode(true);
    }

    public void supprimer() {
        if (selectedObject == null) {
            setMessage(getI18nMessage("msg.action.delete.error.noselect"));
        } else {
            try {
                Integer id = selectedObject.getIdspec();
                service.delete(selectedObject, loginBean.getConnectedUser(), selectedObject.toString());
                dataList.remove(index);
                effacer();
                setMessage(getI18nMessage("msg.action.delete.success", id));
                showSuccesOppDialog();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.delete.fail", selectedObject.getIdspec(), ex.getMessage()));
                Logger.getLogger(SpecialiteBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    //Confirmer la suppression
    public void confirmerSuppression() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confirmDelete').show();");
    }

    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOpp').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOpp').show();");
    }

    public void rechercher(boolean searchByAnd) {
        Map<String, Object> params = new HashMap<>();
        if (formObject.getIdspec() != null && formObject.getIdspec().toString().length() > 0 && !"%".equals(formObject.getIdspec().toString())) {
            params.put("idspec", formObject.getIdspec());
        }
        if (formObject.getLibspec() != null && formObject.getLibspec().length() > 0 && !"%".equals(formObject.getLibspec())) {
            params.put("libspec", formObject.getLibspec());
        }
        dataList.clear();
        try {
            dataList.addAll(service.findBy(params, (searchByAnd ? OperateurSql.AND : OperateurSql.OR)));
            if (dataList.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(SpecialiteBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the service
     */
    public SpecialiteService getService() {
        return service;
    }

    /**
     * @return the formObject
     */
    public Specialite getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
    public void setFormObject(Specialite formObject) {
        this.formObject = formObject;
    }

    /**
     * @return the selectedObject
     */
    public Specialite getSelectedObject() {
        return selectedObject;
    }

    /**
     * @param selectedObject the selectedObject to set
     */
    public void setSelectedObject(Specialite selectedObject) {
        this.selectedObject = selectedObject;
    }

    /**
     * @return the dataList
     */
    public List<Specialite> getDataList() {
        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(List<Specialite> dataList) {
        this.dataList = dataList;
    }

    /**
     * @return the filteredList
     */
    public List<Specialite> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Specialite> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }
  /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}
