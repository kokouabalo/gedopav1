package com.mte.gedopa.view.mbeans.gedopa;

import com.mte.gedopa.util.OperateurSql;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import com.mte.gedopa.service.gedopa.MedecinService;
import com.mte.gedopa.service.impl.gedopa.MedecinServiceImpl;
import com.mte.gedopa.entities.Medecin;
import com.mte.gedopa.util.MessageProvider;
import com.mte.gedopa.view.mbeans.util.LoginBean;
import java.text.MessageFormat;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author H
 */
@ManagedBean(name = "medecinBean")
@ViewScoped
public class MedecinBean implements Serializable {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    
    private MedecinService service;

    private int index;
    private Medecin formObject;
    private Medecin selectedObject;
    private List<Medecin> dataList = new ArrayList<>();
    private List<Medecin> filteredList;
    private String message;
    private boolean editMode;

    private String gridEmptyMessage = "";

    @PostConstruct
    public void init() {
        try {
            service = MedecinServiceImpl.getInstance();
            gridEmptyMessage = getI18nMessage("msg.grid.load.empty");
            effacer();
        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(MedecinBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enregistrer() {
        if (isEditMode()) {
            try {
                // Mode modif
                getService().update(getFormObject(), loginBean.getConnectedUser(), formObject.toString());
                dataList.set(index, formObject);
                setMessage(getI18nMessage("msg.action.update.success", formObject.getIdmed()));
                showSuccesOppDialog();
                effacer();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.update.fail", formObject.getIdmed(), ex.getMessage()));
                Logger.getLogger(MedecinBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        } else {
            //Mode ajout
            try {
                Medecin entity = formObject;
                entity = getService().create(entity, loginBean.getConnectedUser(), entity.toString());
                getDataList().add(entity);
                setMessage(getI18nMessage("msg.action.create.success", entity.getIdmed()));
                showSuccesOppDialog();
                effacer();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.create.fail", formObject.getIdmed(), ex.getMessage()));
                Logger.getLogger(MedecinBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    public void effacer() {
        setFormObject(new Medecin());
        setSelectedObject(null);
        setEditMode(false);
    }
    
    public void effacerTout(){
        effacer();
        dataList.clear();
    }

    public void rowSelect() {
        setFormObject(getSelectedObject());
        index = dataList.indexOf(selectedObject);
        setEditMode(true);
    }

    public void supprimer() {
        if (selectedObject == null) {
            setMessage(getI18nMessage("msg.action.delete.error.noselect"));
        } else {
            try {
                String id = selectedObject.getIdmed();
                service.delete(selectedObject, loginBean.getConnectedUser(), selectedObject.toString());
                dataList.remove(index);
                effacer();
                setMessage(getI18nMessage("msg.action.delete.success", id));
                showSuccesOppDialog();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.delete.fail", selectedObject.getIdmed(), ex.getMessage()));
                Logger.getLogger(MedecinBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    //Confirmer la suppression
    public void confirmerSuppression() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confirmDelete').show();");
    }

    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOpp').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOpp').show();");
    }

    public void rechercher(boolean searchByAnd) {
        Map<String, Object> params = new HashMap<>();
        if (formObject.getIdmed() != null && formObject.getIdmed().length() > 0 && !"%".equals(formObject.getIdmed())) {
            params.put("idmed", formObject.getIdmed());
        }
        if (formObject.getNommedecin() != null && formObject.getNommedecin().length() > 0 && !"%".equals(formObject.getNommedecin())) {
            params.put("nommedecin", formObject.getNommedecin());
        }
        if (formObject.getPrenommedecin() != null && formObject.getPrenommedecin().length() > 0 && !"%".equals(formObject.getPrenommedecin())) {
            params.put("prenommedecin", formObject.getPrenommedecin());
        }
        dataList.clear();
        try {
            dataList.addAll(service.findBy(params, (searchByAnd ? OperateurSql.AND : OperateurSql.OR)));
            if (dataList.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(MedecinBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the service
     */
    public MedecinService getService() {
        return service;
    }

    /**
     * @return the formObject
     */
    public Medecin getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
    public void setFormObject(Medecin formObject) {
        this.formObject = formObject;
    }

    /**
     * @return the selectedObject
     */
    public Medecin getSelectedObject() {
        return selectedObject;
    }

    /**
     * @param selectedObject the selectedObject to set
     */
    public void setSelectedObject(Medecin selectedObject) {
        this.selectedObject = selectedObject;
    }

    /**
     * @return the dataList
     */
    public List<Medecin> getDataList() {
        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(List<Medecin> dataList) {
        this.dataList = dataList;
    }

    /**
     * @return the filteredList
     */
    public List<Medecin> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Medecin> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }
  /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}
