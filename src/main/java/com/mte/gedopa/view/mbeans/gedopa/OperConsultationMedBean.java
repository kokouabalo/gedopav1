package com.mte.gedopa.view.mbeans.gedopa;

import com.mte.gedopa.dao.PersistenceService;
import com.mte.gedopa.entities.AnalyseDemandee;
import com.mte.gedopa.entities.AnalyseDemandeePK;
import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.util.OperateurSql;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import com.mte.gedopa.service.gedopa.ConsultationService;
import com.mte.gedopa.service.impl.gedopa.ConsultationServiceImpl;
import com.mte.gedopa.entities.Consultation;
import com.mte.gedopa.entities.Hypothesediag;
import com.mte.gedopa.entities.HypothesediagCons;
import com.mte.gedopa.entities.HypothesediagConsPK;
import com.mte.gedopa.util.MessageProvider;
import java.text.MessageFormat;
import java.util.ArrayList;
import com.mte.gedopa.entities.Prescription;
import com.mte.gedopa.entities.PrescriptionPK;
import com.mte.gedopa.entities.Medecin;
import com.mte.gedopa.entities.Typeconsult;
import com.mte.gedopa.service.admin.SysParamService;
import com.mte.gedopa.service.impl.admin.SysParamServiceImpl;
import com.mte.gedopa.service.impl.gedopa.PatientServiceImpl;
import com.mte.gedopa.service.gedopa.PatientService;
import com.mte.gedopa.service.gedopa.MedecinService;
import com.mte.gedopa.service.gedopa.TypeconsultService;
import com.mte.gedopa.service.impl.gedopa.MedecinServiceImpl;
import com.mte.gedopa.service.impl.gedopa.TypeconsultServiceImpl;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import com.mte.gedopa.view.mbeans.util.Invalidator;
import com.mte.gedopa.view.mbeans.util.LoginBean;
import com.mte.gedopa.view.mbeans.util.SearchPatientBean;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author ABALO Kokou 
 */
@ManagedBean(name = "operConsultationMedBean")
@ViewScoped
public class OperConsultationMedBean implements Serializable, PropertyChangeListener {

    @ManagedProperty(value = "#{searchPatientBean}")
    private SearchPatientBean searchPatientBean;
        

    @ManagedProperty(value = "#{prescriptionBean}")
    private PrescriptionBean prescriptionBean;
    
    @ManagedProperty(value = "#{analyseDemandeeBean}")
    private AnalyseDemandeeBean analyseDemandeeBean;
    
    @ManagedProperty(value = "#{hypothesediagConsBean}")
    private HypothesediagConsBean hypothesediagConsBean;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;

    private ConsultationService service;
    private PatientService patientService;
    private MedecinService medecinService;
    private TypeconsultService typeconsultService;
    private SysParamService sps;

    private int index;
    private Consultation formObject;
    private Consultation selectedObject;
    private Patient selectedPatient;
    private List<Consultation> dataList = new ArrayList<>();
    private List<Consultation> toAddList = new ArrayList<>();
    private List<Medecin> medecinList = new ArrayList<>();
    private List<Typeconsult> typeconsultList = new ArrayList<>();
    private List<Patient> annonceEscaleList;
    private List<Consultation> filteredList;
    private String message;
    private boolean editMode;

    private String gridEmptyMessage = "";

    PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    Invalidator invalidate;

    @PostConstruct
    public void init() {
        try {
            service = ConsultationServiceImpl.getInstance();
            patientService = PatientServiceImpl.getInstance();
            typeconsultService = TypeconsultServiceImpl.getInstance();
            medecinService = MedecinServiceImpl.getInstance();
            typeconsultList = typeconsultService.read();
            medecinList = medecinService.read();
            sps = SysParamServiceImpl.getInstance();
            gridEmptyMessage = getI18nMessage("msg.grid.load.empty");

            invalidate = new Invalidator();

            try {
                getSearchPatientBean().addPropertyChangeListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            effacer();

        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enregistrer() {
        if (isEditMode()) {
            try {
                // Mode modif
                getService().update(getFormObject(), getLoginBean().getConnectedUser(), getFormObject().toString());
                dataList.set(index, formObject);

                setMessage(getI18nMessage("msg.action.update.success", formObject.getIdconsultaion()));
                showSuccesOppDialog();

                effacer();
            } catch (Exception ex) {
               // setMessage(getI18nMessage("msg.action.update.fail", formObject.getIdconsultaion(), ex.getMessage()));
                Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        } else {
            //Mode ajout
            if (formObject.getId_patient() == null) {
                Invalidator.invalide("CONSULTATION/PATIENT : ", "Vous devez sélectionner un patient.");
            } else {
                try {
                    Consultation entity = formObject;
                    entity = getService().create(entity, getLoginBean().getConnectedUser(), entity.toString());
                    getDataList().add(entity);
                    setMessage(getI18nMessage("msg.action.create.success", entity.getIdconsultaion()));
                    showSuccesOppDialog();

                    effacer();
                } catch (Exception ex) {
                    setMessage(getI18nMessage("msg.action.create.fail", formObject.getIdconsultaion(), ex.getMessage()));
                    Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
                    showEchecOppDialog();
                }
            }
        }
    }

    public void enregistrerConsultation() {
        //Permet de renvoyer un message à  l'utilisateur en cas de non selection d'un patient
        if (formObject.getId_patient() == null || formObject.getId_patient().getId_patient() == null) {
            invalidate("Dossier patient : ", "Vous n'avez sélectionné aucun patient!");
            return;
        }
        if (prescriptionBean.getDataListDetail().isEmpty()||analyseDemandeeBean.getDataListDetail().isEmpty()||hypothesediagConsBean.getDataListDetail().isEmpty()) {
            invalidate("Détails consultation : ", "Vous n'avez saisi aucun détail pour cette consultation !");
            return;
        }
        if (isEditMode()) {
            try {
                // Mode modif
                PersistenceService.getInstance().beginTx();
                getService().update(getFormObject(), false, getLoginBean().getConnectedUser(), getFormObject().toString());
                dataList.set(index, formObject);
                enregistrerDetailCons(formObject, false); //

                PersistenceService.getInstance().commitTx();
                setMessage(getI18nMessage("msg.action.update.success", formObject.getIdconsultaion()));
                showSuccesOppDialogMseManut();

                effacer();
                clearDetailLists();
                formObject.setId_patient(getSelectedPatient());
            } catch (Exception ex) {
                try {
                    PersistenceService.getInstance().rollbackTx();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setMessage(getI18nMessage("msg.action.update.fail", formObject.getIdconsultaion(), ex.getMessage()));
                Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        } else {
            //Mode ajout
            try {

                Consultation entityConsultation = formObject;
                //entityConsultation.setDateEquipe(Calendar.getInstance().getTime());

                PersistenceService.getInstance().beginTx();
                //entityConsultation.setDateDmdFact(null);
                entityConsultation = getService().create(entityConsultation, false, getLoginBean().getConnectedUser(), entityConsultation.toString());
                //on enregistre les détails
                enregistrerDetailCons(entityConsultation, false);
                PersistenceService.getInstance().commitTx();
                getDataList().add(0, entityConsultation);

                setMessage(getI18nMessage("msg.action.create.success", entityConsultation.getIdconsultaion()));
                showSuccesOppDialogMseManut();

                effacer();
                clearDetailLists();
                formObject.setId_patient(getSelectedPatient());
            } catch (Exception ex) {
                try {
                    PersistenceService.getInstance().rollbackTx();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setMessage(getI18nMessage("msg.action.create.fail", formObject.getIdconsultaion(), ex.getMessage()));
                Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    private void enregistrerDetailCons(Consultation master, boolean autoCommit) throws Exception {

        // Pour les détails des médicaments
        for (Prescription detailPresciption : prescriptionBean.getToAddList()) {
            detailPresciption.setConsultation(master);
            detailPresciption.setPrescriptionPK(new PrescriptionPK(detailPresciption.getMedicament().getCodemedi(),master.getIdconsultaion()));
            detailPresciption = prescriptionBean.getServiceDetail().create(detailPresciption, autoCommit, getLoginBean().getConnectedUser(), detailPresciption.toString());
        }
        // Pour les détails des hypothèses diagnostics
        for (HypothesediagCons detailHypothesediagCons : hypothesediagConsBean.getToAddList()) {
            detailHypothesediagCons.setConsultation(master);
            detailHypothesediagCons.setHypothesediagConsPK(new HypothesediagConsPK(detailHypothesediagCons.getHypothesediag().getIdhypo(),master.getIdconsultaion()));
            detailHypothesediagCons = hypothesediagConsBean.getServiceDetail().create(detailHypothesediagCons, autoCommit, getLoginBean().getConnectedUser(), detailHypothesediagCons.toString());
        }
        
        // Pour les détails des analyses  
        for (AnalyseDemandee detailAnalyseDemandee : analyseDemandeeBean.getToAddList()) {
            detailAnalyseDemandee.setConsultation(master);
            detailAnalyseDemandee.setAnalyseDemandeePK(new AnalyseDemandeePK(detailAnalyseDemandee.getAnalysemed().getIdanamed(),master.getIdconsultaion()));
            detailAnalyseDemandee = analyseDemandeeBean.getServiceDetail().create(detailAnalyseDemandee, autoCommit, getLoginBean().getConnectedUser(), detailAnalyseDemandee.toString());
        }
    }

    public void effacer() {
        setFormObject(new Consultation());
        setSelectedObject(null);
        setEditMode(false);
    }

    public void effacerDetail() {
        try{
        prescriptionBean.effacerDetail();
        prescriptionBean.getToAddList().clear();
        prescriptionBean.getDataListDetail().clear();
        prescriptionBean.getMedicamentList().clear();
        prescriptionBean.getMedicamentList().addAll(prescriptionBean.getMedicamentService().read());
        
       } catch(Exception e){
            e.printStackTrace();
        }
    }

    public void effacerMasterDet() {
        effacer();
        effacerDetail();
    }

    private void clearDetailLists() {
        prescriptionBean.getToAddList().clear();
        prescriptionBean.getDataListDetail().clear();
        //
        hypothesediagConsBean.getToAddList().clear();
        hypothesediagConsBean.getDataListDetail().clear();
        //
        analyseDemandeeBean.getToAddList().clear();
        analyseDemandeeBean.getDataListDetail().clear();
    }

    public void rowSelect() {
        //Pour la liste déroulante des consignataires
       // rechercherManut(selectedObject.getIdPatient());///
        try {
            setFormObject(service.getDao().clone(getSelectedObject()));
        } catch (Exception ex) {
            Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        index = dataList.indexOf(selectedObject);
        setEditMode(true);

    }

    public void rowSelectReloadDetConsultation() {
        rowSelect();
        reloadDetailsOperConsultationMed();
    }

    public void reloadDetailsOperConsultationMed() {
        //on recharge les détails des marchandises manutentionnées
        searchPatientBean.effacer();
        searchPatientBean.getDataList().clear();
        prescriptionBean.rechercherDetail(selectedObject);
        hypothesediagConsBean.rechercherDetail(selectedObject);
        analyseDemandeeBean.rechercherDetail(selectedObject);
    }
    

    //Confirmer la suppression
    public void confirmerSuppression() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confirmDelete').show();");
    }

    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOpp').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOpp').show();");
    }

    public void showSuccesOppDialogMseManut() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOppMseManut').show();");
    }
    public void ctrlManutEscaleDialog(){
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ctrlManutEscale').show();");
    }

    public void rechercher(boolean searchByAnd) {
        Map<String, Object> params = new HashMap<>();
        if (formObject.getIdconsultaion() != null) {
            params.put("idConsultation", formObject.getIdconsultaion());
        }
        dataList.clear();
        try {
            dataList.addAll(service.findBy(params, (searchByAnd ? OperateurSql.AND : OperateurSql.OR)));
            if (dataList.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }

    public void supprimer() {
        if (selectedObject == null) {
            setMessage(getI18nMessage("msg.action.delete.error.noselect"));
        } else {
            try {

                for (Prescription detDmdFrnt : prescriptionBean.getDataListDetail()) {
                    prescriptionBean.getServiceDetail().delete(detDmdFrnt, getLoginBean().getConnectedUser(), detDmdFrnt.toString());
                }
                  
             /*   for (TonnageTranspConc tnt : tonnageTranspConcBean.getDataListDetail()) {
                    tonnageTranspConcBean.getServiceDetail().delete(tnt, getLoginBean().getConnectedUser(), tnt.toString());
                }
                
                for (EquipeManutConc eqManut : equipeManutConcBean.getDataListDetailEquipe()) {
                    equipeManutConcBean.getServiceDetail().delete(eqManut, getLoginBean().getConnectedUser(), eqManut.toString());
                }
              */

                Long id = selectedObject.getIdconsultaion();
                service.delete(selectedObject, getLoginBean().getConnectedUser(), selectedObject.toString());
                
                
                dataList.remove(index);

                effacer();
                setMessage(getI18nMessage("msg.action.delete.success", id));
                showSuccesOppDialog();
            } catch (Exception ex) {
                setMessage(getI18nMessage("msg.action.delete.fail", selectedObject.getIdconsultaion(), ex.getMessage()));
                Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
                showEchecOppDialog();
            }
        }
    }

    public void rechercherConsultation() {
        // Liste de toutes les demandes sur l'escale
        try {
        List<Critere> criteres = new ArrayList<>();
        

        criteres.add(new Critere("dateDmdFact", OperateurCond.EQUAL, null, OperateurSql.AND));
        criteres.add(new Critere("manutentionnaire.idClient", OperateurCond.NOT_EQUAL, Long.valueOf(sps.getValue("ID_MANUT_PORT")), OperateurSql.AND));

        dataList.clear();
        getToAddList().clear();
        
            dataList.addAll(service.findBy(criteres));
            toAddList.addAll(dataList);

            if (dataList.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }

            System.out.println("rechercherConsultation()");

        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }

    }
/*
    public void rechercherManut(Patient escale) {
        try {
             getManutentionnaireList().clear();
             getManutentionnaireTmpList().clear();
            if (escale != null) {
             getManutentionnaireTmpList().addAll(ligneMseService.listeManutentionnaire(escale));
             if((getManutentionnaireTmpList().isEmpty())|| (getManutentionnaireTmpList().get(0)) == null){
                Invalidator.invalide("MANUTENTIONNAIRE : ", "L'escale sélectionnée n'a pas de manutentionnaire.");
               // setMessage(getI18nMessage("msg.erreur.manut.escale"));
              //  ctrlManutEscaleDialog();
              }else{
                getManutentionnaireList().addAll(getManutentionnaireTmpList());
             }
            }
        } catch (Exception ex) {
          //  Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, null, ex);
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
           
        }
    }
    */
    /*
    public void rechercherParAnnonce(boolean searchByAnd) {
        Map<String, Object> params = new HashMap<>();
        if (formObject.getIdPatient() != null) {
            params.put("idPatient.idPatient", formObject.getIdPatient().getIdPatient());
        }
        dataList.clear();
        try {
            dataList.addAll(service.findBy(params, (searchByAnd ? OperateurSql.AND : OperateurSql.OR)));
            if (dataList.isEmpty()) {
                gridEmptyMessage = getI18nMessage("msg.grid.search.empty");
            }
        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(OperConsultationMedBean.class.getName()).log(Level.SEVERE, message, ex);
            showEchecOppDialog();
        }
    }
*/
    private void invalidate(String summary, String detail) {
        FacesContext fc = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage();
        fm.setSummary(summary);
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        fc.addMessage(null, fm);
        fc.renderResponse();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectedPatient".equalsIgnoreCase(evt.getPropertyName())) {
            if (evt.getNewValue() instanceof Patient) {
                setSelectedPatient((Patient) evt.getNewValue());
                effacerMasterDet();
                
                formObject.setId_patient((Patient) evt.getNewValue());
                  
            }
        }
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the service
     */
    public ConsultationService getService() {
        return service;
    }

    /**
     * @return the formObject
     */
    public Consultation getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
    public void setFormObject(Consultation formObject) {
        this.formObject = formObject;
    }

    /**
     * @return the selectedObject
     */
    public Consultation getSelectedObject() {
        return selectedObject;
    }

    /**
     * @param selectedObject the selectedObject to set
     */
    public void setSelectedObject(Consultation selectedObject) {
        Consultation oldValue = this.selectedObject;
        this.selectedObject = selectedObject;
        pcs.firePropertyChange("selectedConsultation", oldValue, selectedObject);
    }

    public void addPropertyChangeListener(final PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(final PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /**
     * @return the dataList
     */
    public List<Consultation> getDataList() {
        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(List<Consultation> dataList) {
        this.dataList = dataList;
    }

    /**
     * @return the filteredList
     */
    public List<Consultation> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Consultation> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }

    /**
     * @return the tonnageTranspConcBean
     */
  /*  public TonnageTranspConcBean getTonnageTranspConcBean() {
        return tonnageTranspConcBean;
    }

    /**
     * @param tonnageTranspConcBean the tonnageTranspConcBean to set
     */
  /*  public void setTonnageTranspConcBean(TonnageTranspConcBean tonnageTranspConcBean) {
        this.tonnageTranspConcBean = tonnageTranspConcBean;
    }*/

    /**
     * @return the prescriptionBean
     */
    public PrescriptionBean getPrescriptionBean() {
        return prescriptionBean;
    }

    /**
     * @param prescriptionBean the prescriptionBean to set
     */
    public void setPrescriptionBean(PrescriptionBean prescriptionBean) {
        this.prescriptionBean = prescriptionBean;
    }

    /**
     * @return the annonceEscaleList
     */
    public List<Patient> getPatientList() {
        return annonceEscaleList;
    }

    /**
     * @param annonceEscaleList the annonceEscaleList to set
     */
    public void setPatientList(List<Patient> annonceEscaleList) {
        this.annonceEscaleList = annonceEscaleList;
    }

    /**
     * @return the patientService
     */
    public PatientService getPatientService() {
        return patientService;
    }

    /**
     * @param patientService the patientService to set
     */
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    /**
     * @return the toAddList
     */
    public List<Consultation> getToAddList() {
        return toAddList;
    }

    /**
     * @param toAddList the toAddList to set
     */
    public void setToAddList(List<Consultation> toAddList) {
        this.toAddList = toAddList;
    }

    /**
     * @return the sps
     */
    public SysParamService getSps() {
        return sps;
    }

    /**
     * @param sps the sps to set
     */
    public void setSps(SysParamService sps) {
        this.sps = sps;
    }

    /**
     * @return the selectedPatient
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * @param selectedPatient the selectedPatient to set
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * @return the SearchPatientBean
     */
    public SearchPatientBean getSearchPatientBean() {
        return searchPatientBean;
    }

    /**
     * @param searchPatientBean the SearchPatientBean to set
     */
    public void setSearchPatientBean(SearchPatientBean searchPatientBean) {
        this.searchPatientBean = searchPatientBean;
    }

    /**
     * @return the medecinList
     */
    public List<Medecin> getMedecinList() {
        return medecinList;
    }

    /**
     * @param medecinList the medecinList to set
     */
    public void setMedecinList(List<Medecin> medecinList) {
        this.medecinList = medecinList;
    }

    /**
     * @return the typeconsultList
     */
    public List<Typeconsult> getTypeconsultList() {
        return typeconsultList;
    }

    /**
     * @param typeconsultList the typeconsultList to set
     */
    public void setTypeconsultList(List<Typeconsult> typeconsultList) {
        this.typeconsultList = typeconsultList;
    }

    /**
     * @return the typeconsultService
     */
    public TypeconsultService getTypeconsultService() {
        return typeconsultService;
    }

    /**
     * @param typeconsultService the typeconsultService to set
     */
    public void setTypeconsultService(TypeconsultService typeconsultService) {
        this.typeconsultService = typeconsultService;
    }

    /**
     * @return the analyseDemandeeBean
     */
    public AnalyseDemandeeBean getAnalyseDemandeeBean() {
        return analyseDemandeeBean;
    }

    /**
     * @param analyseDemandeeBean the analyseDemandeeBean to set
     */
    public void setAnalyseDemandeeBean(AnalyseDemandeeBean analyseDemandeeBean) {
        this.analyseDemandeeBean = analyseDemandeeBean;
    }

    /**
     * @return the hypothesediagConsBean
     */
    public HypothesediagConsBean getHypothesediagConsBean() {
        return hypothesediagConsBean;
    }

    /**
     * @param hypothesediagConsBean the hypothesediagConsBean to set
     */
    public void setHypothesediagConsBean(HypothesediagConsBean hypothesediagConsBean) {
        this.hypothesediagConsBean = hypothesediagConsBean;
    }
    
}
