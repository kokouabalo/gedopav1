/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.mbeans.util;

import com.mte.gedopa.entities.Medecin;
import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.entities.Typeconsult;
import com.mte.gedopa.service.gedopa.MedecinService;
import com.mte.gedopa.service.gedopa.PatientService;
import com.mte.gedopa.service.gedopa.TypeconsultService;
import com.mte.gedopa.service.impl.gedopa.MedecinServiceImpl;
import com.mte.gedopa.service.impl.gedopa.PatientServiceImpl;
import com.mte.gedopa.service.impl.gedopa.TypeconsultServiceImpl;
import com.mte.gedopa.util.MessageProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.context.RequestContext;

/**
 *
 * @author HP
 */

@ManagedBean(name = "infoPatientBean")
@ViewScoped
public class InfoPatientBean implements Serializable, PropertyChangeListener {

    @ManagedProperty(value = "#{searchPatientRecordBean}")
    private SearchPatientRecordBean searchPatientRecordBean;
   
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;

    private PatientService service;
    private PatientService patientService;
    private MedecinService medecinService;
    private TypeconsultService typeconsultService;
  
    private int index;
    private Patient formObject;
    private Patient selectedObject;
    private Patient selectedPatient;
    private List<Patient> dataList = new ArrayList<>();
    private List<Patient> toAddList = new ArrayList<>();
    private List<Medecin> medecinList = new ArrayList<>();
    private List<Typeconsult> typeconsultList = new ArrayList<>();
    private List<Patient> filteredList;
    private boolean renderedInfosPatient = true;
    private boolean collapseInfos = true;
    private String infoAffichage = "0";
    
    private String message;
    private boolean editMode;

    private String gridEmptyMessage = "";

    PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    Invalidator invalidate;

    @PostConstruct
    public void init() {
        try {
            service = PatientServiceImpl.getInstance();
            patientService = PatientServiceImpl.getInstance();
            typeconsultService = TypeconsultServiceImpl.getInstance();
            medecinService = MedecinServiceImpl.getInstance();
            typeconsultList = typeconsultService.read();
            medecinList = medecinService.read();
            //sps = SysParamServiceImpl.getInstance();
            gridEmptyMessage = getI18nMessage("msg.grid.load.empty");
            FacesContext contexte = FacesContext.getCurrentInstance();
            infoAffichage = contexte.getExternalContext().getRequestParameterMap().get("infoAffichage");
            HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
             
            if ("1".equals(infoAffichage)) {
                setCollapseInfos(false);
            }

            invalidate = new Invalidator();

            try {
                getSearchPatientRecordBean().addPropertyChangeListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            effacer();

        } catch (Exception ex) {
            this.message = getI18nMessage("error.app.init", ex.getMessage());
            Logger.getLogger(InfoPatientBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void effacer() {
        setFormObject(new Patient());
        setSelectedObject(null);
        setEditMode(false);
    }

    public void rowSelect() {
        try {
            setFormObject(service.getDao().clone(getSelectedObject()));
        } catch (Exception ex) {
            Logger.getLogger(InfoPatientBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        index = dataList.indexOf(selectedObject);
        setEditMode(true);

    }
  
    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOpp').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOpp').show();");
    }

    public void showSuccesOppDialogMseManut() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOppMseManut').show();");
    }
    public void ctrlManutEscaleDialog(){
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ctrlManutEscale').show();");
    }

    private void invalidate(String summary, String detail) {
        FacesContext fc = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage();
        fm.setSummary(summary);
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        fc.addMessage(null, fm);
        fc.renderResponse();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("selectedPatient".equalsIgnoreCase(evt.getPropertyName())) {
            if (evt.getNewValue() instanceof Patient) {
                setSelectedPatient((Patient) evt.getNewValue());
            }
        }
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the service
     */
    public PatientService getService() {
        return service;
    }

    /**
     * @return the formObject
     */
    public Patient getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
    public void setFormObject(Patient formObject) {
        this.formObject = formObject;
    }

    /**
     * @return the selectedObject
     */
    public Patient getSelectedObject() {
        return selectedObject;
    }

    /**
     * @param selectedObject the selectedObject to set
     */
    public void setSelectedObject(Patient selectedObject) {
        Patient oldValue = this.selectedObject;
        this.selectedObject = selectedObject;
        pcs.firePropertyChange("selectedPatient", oldValue, selectedObject);
    }

    public void addPropertyChangeListener(final PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(final PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /**
     * @return the dataList
     */
    public List<Patient> getDataList() {
        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(List<Patient> dataList) {
        this.dataList = dataList;
    }

    /**
     * @return the filteredList
     */
    public List<Patient> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Patient> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }

   
    /**
     * @return the patientService
     */
    public PatientService getPatientService() {
        return patientService;
    }

    /**
     * @param patientService the patientService to set
     */
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    /**
     * @return the toAddList
     */
    public List<Patient> getToAddList() {
        return toAddList;
    }

    /**
     * @param toAddList the toAddList to set
     */
    public void setToAddList(List<Patient> toAddList) {
        this.toAddList = toAddList;
    }


    /**
     * @return the selectedPatient
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * @param selectedPatient the selectedPatient to set
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * @return the SearchPatientBean
     */

    /**
     * @return the medecinList
     */
    public List<Medecin> getMedecinList() {
        return medecinList;
    }

    /**
     * @param medecinList the medecinList to set
     */
    public void setMedecinList(List<Medecin> medecinList) {
        this.medecinList = medecinList;
    }

    /**
     * @return the typeconsultList
     */
    public List<Typeconsult> getTypeconsultList() {
        return typeconsultList;
    }

    /**
     * @param typeconsultList the typeconsultList to set
     */
    public void setTypeconsultList(List<Typeconsult> typeconsultList) {
        this.typeconsultList = typeconsultList;
    }

    /**
     * @return the typeconsultService
     */
    public TypeconsultService getTypeconsultService() {
        return typeconsultService;
    }

    /**
     * @param typeconsultService the typeconsultService to set
     */
    public void setTypeconsultService(TypeconsultService typeconsultService) {
        this.typeconsultService = typeconsultService;
    }

    /**
     * @return the searchPatientRecordBean
     */
    public SearchPatientRecordBean getSearchPatientRecordBean() {
        return searchPatientRecordBean;
    }

    /**
     * @param searchPatientRecordBean the searchPatientRecordBean to set
     */
    public void setSearchPatientRecordBean(SearchPatientRecordBean searchPatientRecordBean) {
        this.searchPatientRecordBean = searchPatientRecordBean;
    }

    /**
     * @return the renderedInfosPatient
     */
    public boolean isRenderedInfosPatient() {
        return renderedInfosPatient;
    }

    /**
     * @param renderedInfosPatient the renderedInfosPatient to set
     */
    public void setRenderedInfosPatient(boolean renderedInfosPatient) {
        this.renderedInfosPatient = renderedInfosPatient;
    }

    /**
     * @return the collapseInfos
     */
    public boolean isCollapseInfos() {
        return collapseInfos;
    }

    /**
     * @param collapseInfos the collapseInfos to set
     */
    public void setCollapseInfos(boolean collapseInfos) {
        this.collapseInfos = collapseInfos;
    }
    
}
