/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.mbeans.util;

import com.mte.gedopa.entities.Patient;
import com.mte.gedopa.service.admin.AppSeqService;
import com.mte.gedopa.service.admin.SysParamService;
import com.mte.gedopa.service.impl.admin.SysParamServiceImpl;
import com.mte.gedopa.service.gedopa.PatientService;
import com.mte.gedopa.service.impl.gedopa.PatientServiceImpl;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.MessageProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author H
 */
@ManagedBean(name = "searchPatientRecordBean")
@ViewScoped
public class SearchPatientRecordBean implements Serializable, PropertyChangeListener {

    private PatientService service;
 
    private int index;
    private int indexBis;
    private Patient formObject;
    private Patient formObjectBis;
    private Patient selectedObject;
    private Patient selectedObjectBis;
    private List<Patient> dataList = new ArrayList<>();
    private List<Patient> dataListBis = new ArrayList<>();

    private List<Critere> criteres = new ArrayList<>();
    private List<Patient> filteredList;
    private List<Patient> filteredListBis;
 
    private String message;
    private boolean editMode;

    private String gridEmptyMessage = "";
    private String dureeForfaitSejourRade;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    private Date dateDebut, dateFin;

    @PostConstruct
    public void init() {
        try {
            setService(PatientServiceImpl.getInstance());
            setGridEmptyMessage(getI18nMessage("msg.grid.load.empty"));
      
            effacer();
            effacerBis();

        } catch (Exception ex) {
            this.setMessage(getI18nMessage("error.app.init", ex.getMessage()));
            Logger.getLogger(SearchPatientRecordBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void effacer() {
        dateDebut = null;
        dateFin = null;
        setFormObject(new Patient());
        setSelectedObject(null);
        setEditMode(false);
        dataList.clear();
    }

    public void effacerBis() {

        setFormObjectBis(new Patient());
        setSelectedObjectBis(null);
        dataListBis.clear();
    }

    public void rowSelect() {
        try {
            setFormObject(getSelectedObject());
           // setSelectedNavire(getSelectedObject().getIdNavire());
            index = dataList.indexOf(getSelectedObject());
        } catch (Exception ex) {
            Logger.getLogger(SearchPatientRecordBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        setIndex(getDataList().indexOf(getSelectedObject()));
        setEditMode(true);
    }

    public void rowSelectBis() {
        try {
            setFormObjectBis(getSelectedObjectBis());
          //  setSelectedNavireBis(getSelectedObjectBis().getIdNavire());
            indexBis = dataListBis.indexOf(getSelectedObjectBis());
        } catch (Exception ex) {
            Logger.getLogger(SearchPatientRecordBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        //setIndex(getDataList().indexOf(getSelectedObject()));
        setEditMode(true);
    }

    public void searchPatientPourConsultation() {
        getCriteres().clear();
        try {
            
            if (getFormObject().getNumdossier() != null && getFormObject().getNumdossier().length() > 0 && !"%".equals(formObject.getNumdossier())) {
                getCriteres().add(new Critere("numdossier", getFormObject().getNumdossier()));
                System.out.println("getFormObject().getNumdossier() "+getFormObject().getNumdossier());
            }

            if (getFormObject().getNom() != null && getFormObject().getNom().length() > 0 && !"%".equals(formObject.getNom())) {
                getCriteres().add(new Critere("nom", getFormObject().getNom()));
            }

            if (getFormObject().getPrenom() != null && getFormObject().getPrenom().length() > 0 && !"%".equals(formObject.getPrenom())) {
                getCriteres().add(new Critere("prenom", getFormObject().getPrenom()));
            }
            getDataList().clear();

            getDataList().addAll(getService().findBy(getCriteres()));

            if (getDataList().isEmpty()) {
                setGridEmptyMessage(getI18nMessage("msg.grid.search.empty"));
            }

        } catch (Exception ex) {
            setMessage(getI18nMessage("msg.action.search.fail", ex.getMessage()));
            Logger.getLogger(SearchPatientRecordBean.class.getName()).log(Level.SEVERE, getMessage(), ex);
            showEchecOppDialog();
        }
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    //Confirmer la suppression
    public void confirmerSuppression() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('confirmDelete').show();");
    }

    public void showEchecOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('echecOpp').show();");
    }

    public void showSuccesOppDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('succesOpp').show();");
    }

    /**
     * @return the service
     */
    public PatientService getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(PatientService service) {
        this.service = service;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the formObject
     */
    public Patient getFormObject() {
        return formObject;
    }

    /**
     * @param formObject the formObject to set
     */
    public void setFormObject(Patient formObject) {
        this.formObject = formObject;
    }

    /**
     * @return the selectedObject
     */
    public Patient getSelectedObject() {
        return selectedObject;
    }

    /**
     * @param selectedObject the selectedObject to set
     */
    public void setSelectedObject(Patient selectedObject) {
        Patient oldValue = this.selectedObject;
        this.selectedObject = selectedObject;
        getPcs().firePropertyChange("selectedPatient", oldValue, selectedObject);

    }

    /**
     * @return the dataList
     */
    public List<Patient> getDataList() {
        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(List<Patient> dataList) {
        this.dataList = dataList;
    }

   

    
    /**
     * @return the filteredList
     */
    public List<Patient> getFilteredList() {
        return filteredList;
    }

    /**
     * @param filteredList the filteredList to set
     */
    public void setFilteredList(List<Patient> filteredList) {
        this.filteredList = filteredList;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the editMode
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     * @param editMode the editMode to set
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * @return the gridEmptyMessage
     */
    public String getGridEmptyMessage() {
        return gridEmptyMessage;
    }

    /**
     * @param gridEmptyMessage the gridEmptyMessage to set
     */
    public void setGridEmptyMessage(String gridEmptyMessage) {
        this.gridEmptyMessage = gridEmptyMessage;
    }
 
    public void addPropertyChangeListener(final PropertyChangeListener l) {
        this.getPcs().addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(final PropertyChangeListener l) {
        this.getPcs().removePropertyChangeListener(l);
    }
 
    /**
     * @return the criteres
     */
    public List<Critere> getCriteres() {
        return criteres;
    }

    /**
     * @param criteres the criteres to set
     */
    public void setCriteres(List<Critere> criteres) {
        this.criteres = criteres;
    }
    /**
     * @return the dureeForfaitSejourRade
     */
    public String getDureeForfaitSejourRade() {
        return dureeForfaitSejourRade;
    }

    /**
     * @param dureeForfaitSejourRade the dureeForfaitSejourRade to set
     */
    public void setDureeForfaitSejourRade(String dureeForfaitSejourRade) {
        this.dureeForfaitSejourRade = dureeForfaitSejourRade;
    }

    /**
     * @return the pcs
     */
    public PropertyChangeSupport getPcs() {
        return pcs;
    }

    /**
     * @param pcs the pcs to set
     */
    public void setPcs(PropertyChangeSupport pcs) {
        this.pcs = pcs;
    }

  
    public void showWarningDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('warningOpp').show();");
    }

    /**
     * @return the formObjectBis
     */
    public Patient getFormObjectBis() {
        return formObjectBis;
    }

    /**
     * @param formObjectBis the formObjectBis to set
     */
    public void setFormObjectBis(Patient formObjectBis) {
        this.formObjectBis = formObjectBis;
    }

    /**
   
    /**
     * @return the selectedObjectBis
     */
    public Patient getSelectedObjectBis() {
        return selectedObjectBis;
    }

    /**
     * @param selectedObjectBis the selectedObjectBis to set
     */
    public void setSelectedObjectBis(Patient selectedObjectBis) {
        this.selectedObjectBis = selectedObjectBis;
    }

    /**
     * @return the dataListBis
     */
    public List<Patient> getDataListBis() {
        return dataListBis;
    }

    /**
     * @param dataListBis the dataListBis to set
     */
    public void setDataListBis(List<Patient> dataListBis) {
        this.dataListBis = dataListBis;
    }

    /**
     * @return the filteredListBis
     */
    public List<Patient> getFilteredListBis() {
        return filteredListBis;
    }

    /**
     * @param filteredListBis the filteredListBis to set
     */
    public void setFilteredListBis(List<Patient> filteredListBis) {
        this.filteredListBis = filteredListBis;
    }

    /**
     * @return the dateDebut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * @param dateDebut the dateDebut to set
     */
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * @return the dateFin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * @param dateFin the dateFin to set
     */
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}