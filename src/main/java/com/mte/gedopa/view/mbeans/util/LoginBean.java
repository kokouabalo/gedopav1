package com.mte.gedopa.view.mbeans.util;

import com.mte.gedopa.entities.AppUser;
import com.mte.gedopa.entities.Menu;
import com.mte.gedopa.service.admin.AppUserService;
import com.mte.gedopa.service.admin.MenuService;
import com.mte.gedopa.service.impl.admin.AppUserServiceImpl;
import com.mte.gedopa.service.impl.admin.MenuServiceImpl;
import com.mte.gedopa.util.MessageProvider;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.servlet.http.HttpServletRequest;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private AppUserService appUserService;
    private MenuService menuService;
    private String login = "sysadmin";
    private String password;
    private boolean connected = false;
    private AppUser connectedUser = null;
    private String loginMsg = "Non connecté";
    private String menuHtmlContent = "";

    public LoginBean() {
    }

    @PostConstruct
    public void init() {
        loginMsg = "Non connecté";
        connectedUser = new AppUser();
        appUserService = AppUserServiceImpl.getInstance();
        menuService = MenuServiceImpl.getInstance();
    }

    public String authentifier() {
        String menuPage = null;
        try {
            connectedUser = appUserService.authentifier(login, password, getOperationDesc());
           // connectedUser = new AppUser(login, password,true);
            System.out.println(connectedUser.getUserLogin());
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur survenue lors de l'authentification! ", ""));
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        if (connectedUser != null && connectedUser.getUserLogin() != null) {
            loginMsg = "Connecté : " + login;
            connected = true;
            menuPage = "index.xhtml?faces-redirect=true";
            try {
                initMenu(connectedUser);
            } catch (Exception ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, getI18nMessage("msg.authentication.failed") + " : ", getI18nMessage("msg.loginOrPassIncorrect")));
        }
        return menuPage;
    }

    private void initMenu(AppUser appUser) throws Exception {
        menuHtmlContent = "";
        List<Menu> realList = appUserService.findMenus(appUser, true);
        List<Menu> workingList = new ArrayList<>();
        for (Menu menu : realList) {

            //on l'ajoute au workingList de façon récursive
            menuService.addChildrenToWorkingList(workingList, menu, true);//ici true pour ajouter les enfants du menu aussi au workingList

        }

        //on récupère les menus de niveau 1
        List<Menu> niveauOneMenus = menuService.getTopMenus();
        for (Menu menu : niveauOneMenus) {
            menuHtmlContent = generateHtmlMenuContent(menuHtmlContent, menu, workingList);
        }
    }

    /*
     * Méthode récursive
     */
    private String generateHtmlMenuContent(String htmlContent, Menu menu, List<Menu> workingList) throws Exception {
        String htmlResult = htmlContent;
        if (!workingList.contains(menu)) {
            return htmlResult;
        }
        List<Menu> children = menuService.getChildMenus(menu);
        //s'il n'a pas d'enfant alors on le traite comme un lien simple sinon comme un menu avec contenu
        if (children != null && children.isEmpty()) {
            htmlResult += "                                            <li>\n"
                    + "                                                <a href=\"" + menu.getScreenId().getUrl() + "\">\n";
            htmlResult += "                                            <li>\n"
                    + "                                                <a href=\"" + "operConsultationMedView.xhtml" + "\">\n";
            
            if (menu.getIconCls() != null && !menu.getIconCls().isEmpty()) {
                htmlResult += "                                    <i class=\"" + menu.getIconCls() + "\"></i>\n";
            }
            if (menu.getMenuParent() != null) {//s'il est dans un sous-menu
                htmlResult += "                                                    <span class=\"menu-text\">" + menu.getMenuDesc() + "</span>\n";

            } else {//sinon il faut l'afficher en gras
                htmlResult += "                                                    <span class=\"menu-text\" style=\"font-weight:bold;\">" + menu.getMenuDesc() + "</span>\n";
            }
            htmlResult += "                                                </a>\n"
                    + "                                            </li>\n";

        } else {//sinon donc menu avec contenu
            htmlResult += "                                    <li class=\"hoe-has-menu\">\n"
                    + "                                        <a href=\"javascript:void(0)\">\n";
            if (menu.getIconCls() != null && !menu.getIconCls().isEmpty()) {
                htmlResult += "                                    <i class=\"" + menu.getIconCls() + "\"></i>\n";
            }
            htmlResult += "                                            <span class=\"menu-text\" style=\"font-weight: bold; font-size: " + (menu.getMenuParent() == null ? "13px;" : "12px;") + "\">" + menu.getMenuDesc() + "</span>\n"
                    + "                                        </a>\n"
                    + "                                        <ul class=\"hoe-sub-menu\">\n";

            for (Menu child : children) {
                htmlResult = generateHtmlMenuContent(htmlResult, child, workingList);
            }

            htmlResult += "                                        </ul>\n"
                    + "                                    </li>\n";
        }

        return htmlResult;

    }

    public String showLogin() {
        return "login.xhtml?faces-redirect=true";
    }

    public void redirect(PhaseEvent e) {
        if (!connected || connectedUser == null || connectedUser.getUserLogin() == null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public String deconnecter() {
        if (connected) {
            try {
                appUserService.logout(connectedUser, getOperationDesc());
            } catch (Exception ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        connected = false;
        loginMsg = "Déconnecté depuis " + (new SimpleDateFormat("dd/MM/yy HH:mm:ss")).format(Calendar.getInstance().getTime());
        connectedUser = new AppUser();
        //on efface le menu de l'utilisateur
        menuHtmlContent = "";
        return "login.xhtml?faces-redirect=true";

    }

    public void changePassword() {

    }

    public String getOperationDesc() {
        String operDesc = "POSTE CLIENT = ";
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String remoteHost = request.getRemoteHost();
        if (remoteHost != null && !"UNKNOWN".equalsIgnoreCase(remoteHost)) {
            operDesc += remoteHost;
        } else {
            String originalHostName = request.getHeader("X-FORWARDED-FOR");
            if (originalHostName != null) {
                operDesc += originalHostName;
            } else {
                String remoteAdr = request.getRemoteAddr();
                operDesc += remoteAdr;
            }
        }
        
        String agentInfo = request.getHeader("User-Agent");
        if(agentInfo != null){
            operDesc += " :: OS/BROWSER = " + agentInfo;
        }

        return operDesc;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the loginMsg
     */
    public String getLoginMsg() {
        return loginMsg;
    }

    /**
     * @return the connectedUser
     */
    public AppUser getConnectedUser() {
        return connectedUser;
    }

    /**
     * @param connectedUser the connectedUser to set
     */
    public void setConnectedUser(AppUser connectedUser) {
        this.connectedUser = connectedUser;
    }

    /**
     * @return the menuHtmlContent
     */
    public String getMenuHtmlContent() {
        return menuHtmlContent;
    }

    public String getI18nMessage(String key) {
        return MessageProvider.getInstance().getValue(key);
    }

    public String getI18nMessage(String key, Object... params) {
        String msg = MessageProvider.getInstance().getValue(key);
        return MessageFormat.format(msg, params);
    }

    /**
     * @return the connected
     */
    public boolean isConnected() {
        return connected;
    }

}
