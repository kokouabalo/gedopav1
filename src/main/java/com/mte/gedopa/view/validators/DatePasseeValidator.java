/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.validators;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author HP
 */
@FacesValidator(value = "datePasseeValid")
public class DatePasseeValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String validate = fc.getExternalContext().getRequestParameterMap().get("validate");
        String validate1 = fc.getExternalContext().getRequestParameterMap().get("validate1");
        String validate2 = fc.getExternalContext().getRequestParameterMap().get("validate2");
        String validate3 = fc.getExternalContext().getRequestParameterMap().get("validate3");
        if ((validate == null || "false".equals(validate)) && (validate1 == null || "false".equals(validate1)) && (validate2 == null || "false".equals(validate2)) && (validate3 == null || "false".equals(validate3))) {
            return;
        }
        Date date = null;
        try {
            
            //Si la date est obligatoire, on laisse required faire son job; sinon si elle ne l'estt pas, alors on ne valide rien si elle est  nulle
            if (o == null || String.valueOf(o).isEmpty()) {
                return;
            }
            date = (Date) o;

        } catch (Exception e) {
            Logger.getLogger(DatePasseeValidator.class.getName()).log(Level.SEVERE, null, e);
            String detail = "Erreur survenue lors de la validation de la date et heure " + o + " par rapport la date et heure actuelle. Cause : " + e.getMessage();
            throw new ValidatorException(createFacesMessage(uic, detail));
        }
        
        String dateStr = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format(date);
        Date dateActuelle = Calendar.getInstance().getTime();
        if (date.getTime() > dateActuelle.getTime()) {
            String detail = dateStr + " est postérieure à la date actuelle: " + (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format(dateActuelle);
            throw new ValidatorException(createFacesMessage(uic, detail));
        }
    }

    private FacesMessage createFacesMessage(UIComponent uic, String detail) {
        FacesMessage fm = new FacesMessage();
        fm.setSummary(uic.getId()+ " : ");
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        return fm;
    }

}
