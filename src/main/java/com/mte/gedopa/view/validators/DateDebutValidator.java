/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.calendar.Calendar;

/**
 *
 * @author HP
 */
@FacesValidator(value = "dateDebutValid")
public class DateDebutValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String validate = fc.getExternalContext().getRequestParameterMap().get("validate");
        String validate1 = fc.getExternalContext().getRequestParameterMap().get("validate1");
        String validate2 = fc.getExternalContext().getRequestParameterMap().get("validate2");
        String validate3 = fc.getExternalContext().getRequestParameterMap().get("validate3");
        if ((validate == null || "false".equals(validate)) && (validate1 == null || "false".equals(validate1)) && (validate2 == null || "false".equals(validate2)) && (validate3 == null || "false".equals(validate3))) {
            return;
        }
        Date dateDebut = null;
        Calendar dateFinComponent = null;
        String submittedDateFin = null;
        Date dateFin = null;
        try {
            dateFinComponent = (Calendar) uic.getAttributes().get("dateFin");
            dateFin = dateFinComponent.getValue() != null ? (Date) dateFinComponent.getValue() : null;
            if (dateFin == null) {
                submittedDateFin = dateFinComponent.getSubmittedValue() != null ? (String) dateFinComponent.getSubmittedValue() : null;
                if (submittedDateFin != null && !submittedDateFin.isEmpty()) {
                    String dateFinPattern = dateFinComponent.getPattern();
                    dateFin = (new SimpleDateFormat(dateFinPattern)).parse(submittedDateFin);
                }
            }

            //Si les dates début et fin sont obligatoires, on laisse required faire son job; sinon si elles ne le sont pas, alors on ne valide rien si elles st nulles
            if (o == null || String.valueOf(o).isEmpty() || dateFin == null) {
                return;
            }
            dateDebut = (Date) o;

            String datePart = (new SimpleDateFormat("dd/MM/yyyy")).format(dateDebut);
            String dateDebutStr = (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dateDebut);
            if (datePart == null) {
                String detail = dateDebutStr + " ne respecte pas le format Date/Heure (jj/mm/aaaa hh:mi)";
                throw new ValidatorException(createFacesMessage(uic, detail));
            } else if (dateDebut.getTime() > dateFin.getTime()) {
                String detail = dateDebutStr + " est postérieure à " + dateFinComponent.getId() + " : " + (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dateFin);
                dateFinComponent.setValid(false);
                throw new ValidatorException(createFacesMessage(uic, detail));
            }

        } catch (ValidatorException e) {
            throw e;
        } catch (Exception ex) {
            Logger.getLogger(DateDebutValidator.class.getName()).log(Level.SEVERE, null, ex);
            String detail = "Erreur survenue lors de la validation de la date et heure " + o + " avec la date et heure fin " + dateFin != null ? (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(dateFin) : null + ". Cause : " + ex.getMessage();
            throw new ValidatorException(createFacesMessage(uic, detail));
        }

    }

    private FacesMessage createFacesMessage(UIComponent uic, String detail) {
        FacesMessage fm = new FacesMessage();
        fm.setSummary(uic.getId() + " : ");
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        return fm;
    }

}
