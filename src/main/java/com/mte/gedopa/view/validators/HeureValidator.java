/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.validators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author HP
 */
@FacesValidator(value = "heureValid")
public class HeureValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String validate = fc.getExternalContext().getRequestParameterMap().get("validate");
        String validate1 = fc.getExternalContext().getRequestParameterMap().get("validate1");
        String validate2 = fc.getExternalContext().getRequestParameterMap().get("validate2");
        String validate3 = fc.getExternalContext().getRequestParameterMap().get("validate3");
        if ((validate == null || "false".equals(validate)) && (validate1 == null || "false".equals(validate1)) && (validate2 == null || "false".equals(validate2)) && (validate3 == null || "false".equals(validate3))) {
            return;
        }
        Date dateHeure = null;
        String heurePart = null;
        String dateHeureStr = null;
        try {

            //Si la date est obligatoire, on laisse required faire son job; sinon on ne valide rien si elle est nulle
            if (o == null || String.valueOf(o).isEmpty()) {
                return;
            }
            dateHeure = (Date) o;
            heurePart = (new SimpleDateFormat("HH:mm:ss")).format(dateHeure);
            dateHeureStr = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format(dateHeure);

        } catch (Exception e) {
            Logger.getLogger(HeureValidator.class.getName()).log(Level.SEVERE, null, e);
            String detail = "Erreur survenue lors de la validation de l'heure saisie! ";
            throw new ValidatorException(createFacesMessage(uic, detail));
        }

        if (heurePart == null || "24:00:00".equals(heurePart) || "00:00:00".equals(heurePart)) {
            String detail = " L'heure est obligatoire pour la date saisie " + dateHeureStr;
            throw new ValidatorException(createFacesMessage(uic, detail));
        }
    }

    private FacesMessage createFacesMessage(UIComponent uic, String detail) {
        FacesMessage fm = new FacesMessage();
        fm.setSummary(uic.getId() + " : ");
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        return fm;
    }

}
