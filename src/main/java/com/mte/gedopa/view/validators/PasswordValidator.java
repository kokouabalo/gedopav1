/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.validators;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.password.Password;

/**
 *
 * @author HP
 */
@FacesValidator(value = "passwordValid")
public class PasswordValidator implements Validator {

    private static final String PASSWORD_REGEX = "^((?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%&*]{3,100})$";

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String validate = fc.getExternalContext().getRequestParameterMap().get("validate");
        String validate1 = fc.getExternalContext().getRequestParameterMap().get("validate1");
        String validate2 = fc.getExternalContext().getRequestParameterMap().get("validate2");
        String validate3 = fc.getExternalContext().getRequestParameterMap().get("validate3");
        if ((validate == null || "false".equals(validate)) && (validate1 == null || "false".equals(validate1)) && (validate2 == null || "false".equals(validate2)) && (validate3 == null || "false".equals(validate3))) {
            return;
        }
        
        Password refComponent = null;
        String refVal = null;
        Matcher matcher = null;
        String valeur = null;
        try {
            valeur = String.valueOf(o);
            refComponent = (Password) uic.getAttributes().get("refVal");
            refVal =(refComponent.getSubmittedValue() != null ? ((String) refComponent.getSubmittedValue()) :  refComponent.getValue() != null ? (String) refComponent.getValue() : null);
            //Si les valeurs saisies sont obligatoires, on laisse required faire son job; sinon si elles ne le sont pas, alors on ne valide rien si elles sont nulles
            if (o == null || String.valueOf(o).isEmpty() || refVal == null) {
                return;
            }
            Pattern pattern = Pattern.compile(PASSWORD_REGEX);
            matcher = pattern.matcher(valeur);
        } catch (Exception e) {
            Logger.getLogger(GreaterDecimalValidator.class.getName()).log(Level.SEVERE, null, e);
            String detail = "Erreur survenue lors de la validation de " + uic.getId() +" " + o + " avec la valeur de référence " + (refComponent != null ? refComponent.getId() : "composant inconnu") + " = " + refVal + ". Cause : " + e.getMessage();
            throw new ValidatorException(createFacesMessage(uic, detail));
        }
        //Test de validation
        
        if (matcher != null && !matcher.matches()) {
            FacesMessage fm = new FacesMessage();
            fm.setSummary(uic.getId() + " : ");
            fm.setDetail("Le Mot de passe, " + valeur + ", ne respecte pas le format attendu: au moins un chiffre, au moins une lettre et eventuellement les caractères spéciaux !@#$%&*. ");
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(fm);
        }
        if (!valeur.equals(refVal)) {//si la valeur est # de la val de référence
            String detail = valeur + " est différent de " + refComponent.getId() + " = " + refVal;
            throw new ValidatorException(createFacesMessage(uic, detail));
        }
    }

    private FacesMessage createFacesMessage(UIComponent uic, String detail) {
        FacesMessage fm = new FacesMessage();
        fm.setSummary(uic.getId() + " : ");
        fm.setDetail(detail);
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        return fm;
    }
}
