/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mte.gedopa.view.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author HP
 */
@FacesValidator(value = "numberQuinzeQuatreValid")
public class NumberQuinzeQuatreValidator implements Validator {

    private static final String NUMBER_QUINZE_QUATRE_REGEX = "^\\d{0,11}+(\\.\\d{1,4})?$";

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        String validate = fc.getExternalContext().getRequestParameterMap().get("validate");
        String validate1 = fc.getExternalContext().getRequestParameterMap().get("validate1");
        String validate2 = fc.getExternalContext().getRequestParameterMap().get("validate2");
        String validate3 = fc.getExternalContext().getRequestParameterMap().get("validate3");
        if ((validate == null || "false".equals(validate)) && (validate1 == null || "false".equals(validate1)) && (validate2 == null || "false".equals(validate2)) && (validate3 == null || "false".equals(validate3))) {
            return;
        }
        Matcher matcher = null;
        String valeur = null;
        try {
            valeur = String.valueOf(o);
            //Si les valeurs saisies sont obligatoires, on laisse required faire son job; sinon si elles ne le sont pas, alors on ne valide rien si elles sont nulles
            if (o == null || String.valueOf(o).isEmpty()) {
                return;
            }
            Pattern pattern = Pattern.compile(NUMBER_QUINZE_QUATRE_REGEX);
            matcher = pattern.matcher(valeur);
        } catch (Exception e) {
            FacesMessage fm = new FacesMessage();
            fm.setSummary(uic.getId() + " : ");
            fm.setDetail("Erreur survenue lors de la validation du nombre " + o + " : " + e.getMessage());
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(fm);
        }
        if (matcher != null && !matcher.matches()) {
            FacesMessage fm = new FacesMessage();
            fm.setSummary(uic.getId() + " : ");
            fm.setDetail("Le nombre décimal " + valeur + " ne respecte pas l'exigence des 11 chiffres max, pour la partie entière et 4 chiffres max, pour la partie décimale");
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(fm);
        }
    }

}
