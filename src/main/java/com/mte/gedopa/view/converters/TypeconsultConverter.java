
package com.mte.gedopa.view.converters;

import com.mte.gedopa.entities.Typeconsult;
import com.mte.gedopa.service.gedopa.TypeconsultService;
import com.mte.gedopa.service.impl.gedopa.TypeconsultServiceImpl;
import com.mte.gedopa.util.Critere;
import com.mte.gedopa.util.OperateurCond;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.Id;


/**
 *
 * @author H
 */
@FacesConverter(value="typeconsultConv")
public class TypeconsultConverter implements Converter{

    TypeconsultService service;
    Field idField;
    
    public TypeconsultConverter() {
        service = TypeconsultServiceImpl.getInstance();
        for(Field field : Typeconsult.class.getDeclaredFields()) {
            if(field.isAnnotationPresent(Id.class)) {
                idField = field;
                break;
            }
        }        
    }
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        try {
           /* if (string != null && string.length() > 0) {
                List<Critere> criteres = new ArrayList<>();
                criteres.add(new Critere(idField.getName(), OperateurCond.EQUAL, Integer.valueOf(string), null));
                List<Typeconsult> list = service.findBy(criteres);
                if (list != null && !list.isEmpty()) {
                    return list.get(0);
                }
            }*/
           if (string != null && string.length() > 0) {
                Map<String, Object> params = new HashMap<>();
                params.put(idField.getName(), String.valueOf(string));
                List<Typeconsult> list = service.findBy(params, null);
                if (list != null && !list.isEmpty()) {
                    return list.get(0);
                }
            }

            return null;
        } catch (Exception ex) {
            Logger.getLogger(TypeconsultConverter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(!(o instanceof Typeconsult)) {
            return String.valueOf(o);
        }
        try {
            idField.setAccessible(true);
            String value = String.valueOf(idField.get(o));
            idField.setAccessible(false);
            return  value;
        } catch(SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(TypeconsultConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
}